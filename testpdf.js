var fs = require('fs')
const gm = require('gm').subClass({ imageMagick: true })

var input = __dirname + '/test-2.pdf'

gm(input + '[0]').density(600).write(__dirname + '/output/test_1.png', function (err) {
  if (err) {
    console.log('err', err)
  }
  console.log('done')
})
