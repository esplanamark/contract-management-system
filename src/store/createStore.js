import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import { browserHistory } from 'react-router'
import makeRootReducer from './reducers'
import { updateLocation } from './location'
import persistState from 'redux-localstorage'
import myaxios from 'myaxios'
import axios from 'axios'

export default (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  // const middleware = [thunk]
  const sagaMiddleware = createSagaMiddleware()
  const middleware = [sagaMiddleware]

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = [persistState(['auth'])]
  if (__DEV__) {
    const devToolsExtension = window.devToolsExtension
    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension())
    }
  }
  
  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    makeRootReducer(),
    initialState,
    compose(
      applyMiddleware(...middleware),
      ...enhancers
    )
  )
  store.asyncReducers = {}
  store.asyncSagas = {}
  store.runSaga = (saga) => {
    sagaMiddleware.run(saga)
  }

  // To unsubscribe, invoke `store.unsubscribeHistory()` anytime
  store.unsubscribeHistory = browserHistory.listen(updateLocation(store))

  // store.subscribe(() => {
  //   const authState = store.getState().auth
  //   myaxios.defaults.headers.common['Authorization'] = 'Bearer ' + authState.token
  // })

  axios.interceptors.request.use(function (request) {
    const authState = store.getState().auth
    if (authState.token) {
      request.headers['Authorization'] = `Bearer ${authState.token}`
    }

    return request
  })

  axios.interceptors.response.use(function (response) {
    // Do something with response data
    return response
  }, function (error) {
    if (error.response.status === 401) {
      try {
        store.dispatch({
          type: 'LOGOUT'
        })
      } catch (e) {
        console.log('e', e)
      }
    }
    return Promise.reject(error)
  })

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const reducers = require('./reducers').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}
