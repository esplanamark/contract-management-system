import axios from 'axios'

// var instance = axios.create({
//   baseURL: 'https://some-domain.com/api/',
//   timeout: 1000,
//   headers: {'X-Custom-Header': 'foobar'}
// });
//
// // Set config defaults when creating the instance
// var instance = axios.create({
//   baseURL: 'https://api.example.com'
// });

// Alter defaults after instance has been created
// instance.defaults.headers.common['Authorization'] = AUTH_TOKEN;

const instance = axios.create()
export default instance
