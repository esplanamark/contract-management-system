import React from 'react'

export const SiteLayout = ({ children }) => (
  <div>
    <div className='container'>
      <div className='core-layout__viewport'>
        {children}
      </div>
    </div>
  </div>
)

SiteLayout.propTypes = {
  children : React.PropTypes.element.isRequired
}

export default SiteLayout
