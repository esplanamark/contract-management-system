import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  increment,
  double,
  addContractRequest,
  updateContractRequest,
  fetchContractRequest,
  destroyContractRequest,
  showContractModal,
  hideContractModal,
  showContractDeleteModal,
  hideContractDeleteModal
} from '../modules/counter'
import { logoutRequest } from '../../../modules/auth'

import * as Api from '../../Category/modules/api'

import Counter from '../components/Counter'
/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the counter:   */

class CounterContainer extends Component {
  searchCategories (inputText) {
    return Api.fetchCategories({
      searchText: inputText
    }).then((categories) => {
      const formattedCategories = categories.data.map((category) => {
        return Object.assign({}, category, {
          value: category.id,
          label: category.name
        })
      })

      return {
        options: formattedCategories
      }
    }).catch((e) => {
      console.log('e', e)
    })
  }

  render () {
    let { children, params } = this.props

    let content
    if (children) {
      content = this.props.children
    } else {
      content = <Counter {...this.props} searchCategories={this.searchCategories} />
    }

    return (
      <div>
        <h1>Contracts</h1>
        <hr />
        {content}
      </div>
    )
  }
}

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around increment; the component doesn't care   */

const mapDispatchToProps = {
  increment : () => increment(1),
  doubleAsync : () => double(),
  logout : () => logoutRequest(),
  fetchContract: (...args) => fetchContractRequest(...args),
  addContract: (...args) => addContractRequest(...args),
  updateContract: (...args) => updateContractRequest(...args),
  destroyContract: (...args) => destroyContractRequest(...args),
  fetchContractRequest,
  showContractModal,
  hideContractModal,
  showContractDeleteModal,
  hideContractDeleteModal
}

const mapStateToProps = (state) => ({
  counter : state.counter.counter,
  contracts: state.counter.list,
  contractsTotalRedcord: state.counter.contractsTotalRedcord,
  contractsRecordsFiltered: state.counter.contractsRecordsFiltered,
  isCreateContractSending: state.counter.isCreateContractSending,
  createContractErrorMessage: state.counter.createContractErrorMessage,
  page: state.counter.page,
  pageSize: state.counter.pageSize,
  searchText: state.counter.searchText,
  sortName: state.counter.sortName,
  sortOrder: state.counter.sortOrder,

  contract: state.counter.currentContract,
  isShowContractModal: state.counter.isShowContractModal,
  isShowContractDeleteModal: state.counter.isShowContractDeleteModal
})

/*  Note: mapStateToProps is where you should use `reselect` to create selectors, ie:

    import { createSelector } from 'reselect'
    const counter = (state) => state.counter
    const tripleCount = createSelector(counter, (count) => count * 3)
    const mapStateToProps = (state) => ({
      counter: tripleCount(state)
    })

    Selectors can compute derived data, allowing Redux to store the minimal possible state.
    Selectors are efficient. A selector is not recomputed unless one of its arguments change.
    Selectors are composable. They can be used as input to other selectors.
    https://github.com/reactjs/reselect    */

export default connect(mapStateToProps, mapDispatchToProps)(CounterContainer)
