import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Grid, Row, Col, Thumbnail } from 'react-bootstrap'
import Lightbox from 'react-images'

import { increment, double, fetchContractRequest, fetchContractByIdRequest } from '../../../modules/counter'

const theme = {
  // container
  container: { background: 'rgba(255, 255, 255, 0.9)' },

  // arrows
  arrow: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    fill: '#222',
    opacity: 0.6,
    transition: 'opacity 200ms',

    ':hover': {
      opacity: 1
    }
  },
  arrow__size__medium: {
    borderRadius: 40,
    height: 40,
    marginTop: -20,

    '@media (min-width: 768px)': {
      height: 70,
      padding: 15
    }
  },
  arrow__direction__left: { marginLeft: 10 },
  arrow__direction__right: { marginRight: 10 },

	// header
  close: {
    fill: '#D40000',
    opacity: 0.6,
    transition: 'all 200ms',

    ':hover': {
      opacity: 1
    }
  },

	// footer
  footer: {
    color: 'black'
  },
  footerCount: {
    color: 'rgba(0, 0, 0, 0.6)'
  },

	// thumbnails
  thumbnail: {},
  thumbnail__active: {
    boxShadow: '0 0 0 2px #00D8FF'
  }
}

class ContractDetail extends Component {
  constructor (props) {
    super(props)

    this.state = {
      images: [],
      lightboxIsOpen: false,
      currentImage: 0
    }
  }

  componentDidMount () {
    this.props.fetchContractByIdRequest(this.props.params.contractId)
  }

  imageClick = (imageIndex) => {
    this.setState({
      currentImage: imageIndex,
      lightboxIsOpen: true
    })
  }

  onLightboxClose = () => {
    this.setState({
      lightboxIsOpen: false,
      currentImage: 0
    })
  }

  gotoPrevious = () => {
    this.setState({
      currentImage: this.state.currentImage - 1
    })
  }

  gotoNext = () => {
    this.setState({
      currentImage: this.state.currentImage + 1
    })
  }

  renderFiles = (files) => {
    if (!files) {
      return ''
    }
    return files.map((file, i) => {
      return (
        <Col xs={6} md={6} key={file.id}>
          <div className='thumbnail'>
            <a href='#'
              target='_blank'
              onClick={(e) => {
                e.preventDefault()
                this.imageClick(i)
              }}>
              <img src={file.url} />
            </a>
          </div>
          <div className='caption'>
            <p>{file.ocr_data}</p>
          </div>
        </Col>
      )
    })
  }

  renderPDFs (files) {
    return (
      <table className='table table-hover'>
        <thead>
          <tr>
            <th>#</th>
            <th>Pdf</th>
          </tr>
        </thead>
        <tbody>
          {
            files.map((file, index) => {
              return (
                <tr key={index}>
                  <th scope='row'>{index + 1}</th>
                  <td><a href={file.url} target='_blank'>{file.name}</a></td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
    )
  }
  render () {
    const { contract, isFetchByIdSending } = this.props
    if (!contract || !contract.id || isFetchByIdSending) {
      return <h1>Loading....</h1>
    }

    const { files } = contract
    let images = []
    let _files = files.filter((file) => {
      return file.type === 'image'
    })

    let _pdfs = files.filter((file) => {
      return file.type === 'pdf'
    })

    if (files) {
      images = _files.map((file) => ({
        src: file.url
        // caption: file.ocr_data
      }))
    }

    return (
      <div>
        <div>Name: {contract.name} <br /><br /></div>
        <div>Description: <p>{contract.Description}</p> <br /><br /></div>
        <h3>PDF Files</h3>
        {this.renderPDFs(_pdfs)}
        <h3>Images</h3>
        <Grid>
          <Row>
            {this.renderFiles(_files)}
          </Row>
        </Grid>
        <Lightbox
          theme={theme}
          images={images}
          isOpen={this.state.lightboxIsOpen}
          onClose={this.onLightboxClose}
          currentImage={this.state.currentImage}
          onClickPrev={this.gotoPrevious}
          onClickNext={this.gotoNext}
          // onClose={this.closeLightbox}
        />
      </div>
    )
  }
}

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around increment; the component doesn't care   */

const mapDispatchToProps = {
  increment : () => increment(1),
  doubleAsync : () => double(),
  fetchContract: (params) => fetchContractRequest(params),
  fetchContractByIdRequest
  // fetchContractById: (...args) => fetchContractByIdRequest(...args)
}

const mapStateToProps = (state) => ({
  counter : state.counter.counter,
  contracts: state.counter.list,
  contract: state.counter.currentContract,
  isFetchByIdSending: state.counter.isFetchByIdSending
})

/*  Note: mapStateToProps is where you should use `reselect` to create selectors, ie:

    import { createSelector } from 'reselect'
    const counter = (state) => state.counter
    const tripleCount = createSelector(counter, (count) => count * 3)
    const mapStateToProps = (state) => ({
      counter: tripleCount(state)
    })

    Selectors can compute derived data, allowing Redux to store the minimal possible state.
    Selectors are efficient. A selector is not recomputed unless one of its arguments change.
    Selectors are composable. They can be used as input to other selectors.
    https://github.com/reactjs/reselect    */

export default connect(mapStateToProps, mapDispatchToProps)(ContractDetail)
