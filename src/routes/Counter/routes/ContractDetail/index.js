export default (store) => ({
  path: '/contracts/:contractId',
  getComponent (nextState, next) {
    require.ensure([
      './containers/ContractDetailContainer'
    ], (require) => {
      const ContractDetailContainer = require('./containers/ContractDetailContainer').default
      next(null, ContractDetailContainer)
    })
  }
})
