export default (store) => ({
  path: '/contracts/new',
  getComponent (nextState, next) {
    require.ensure([
      './containers/ContractNewContainer'
    ], (require) => {
      const ContractNewContainer = require('./containers/ContractNewContainer').default
      next(null, ContractNewContainer)
    })
  }
})
