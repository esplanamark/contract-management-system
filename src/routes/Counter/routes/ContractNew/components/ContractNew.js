import React, { Component } from 'react'
import {
  Button,
  ControlLabel,
  FormGroup,
  FormControl,
  HelpBlock,
  Popover,
  Tooltip,
  Modal,
  OverlayTrigger,
  Row,
  Grid,
  Col
} from 'react-bootstrap'
import Select from 'react-select'

// FIXME:: look for better way todo this
import { autoCompleteCategories } from 'routes/Category/modules/api'
import EmployeeContract from './EmployeeContract'

class ContractNew extends Component {
  constructor (props) {
    super(props)

    this.state = {
      name: '',
      description: '',
      expire_at: '',

      category_id: '',
      html: '',
      raw: '',
      selectedCategory: {}
    }
  }

  handleChange = (e) => {
    this.setState({
      name: e.target.value
    })
  }

  handleDescriptionChange = (e) => {
    this.setState({
      description: e.target.value
    })
  }

  handleExpireAtChange = (e) => {
    this.setState({
      expire_at: e.target.value
    })
  }

  handleLodOptions = (searchText) => {
    return autoCompleteCategories(searchText)
  }

  renderTemplate (categoryId, html) {
    return <EmployeeContract ref={(editor) => { this.editor = editor }} html={html} />
  }

  render () {
    return (
      <div>
        <Grid>
          <Row className='show-grid'>
            <Col xs={12} md={8}>
              <form>
                <FormGroup
                  controlId='formContractName'
                >
                  <ControlLabel>Name</ControlLabel>
                  <FormControl
                    type='text'
                    value={this.state.name}
                    placeholder='Contract name'
                    onChange={this.handleChange}
                  />
                  <FormControl.Feedback />
                </FormGroup>

                <FormGroup
                  controlId='formContractDescription'
                >
                  <ControlLabel>Description</ControlLabel>
                  <FormControl
                    componentClass='textarea'
                    placeholder='textarea'
                    value={this.state.description}
                    placeholder='Description'
                    onChange={this.handleDescriptionChange}
                  />
                  <FormControl.Feedback />
                </FormGroup>

                <FormGroup
                  controlId='formContractName'
                >
                  <ControlLabel>Expire At</ControlLabel>
                  <FormControl
                    type='date'
                    value={this.state.expire_at}
                    placeholder='Expiration date'
                    onChange={this.handleExpireAtChange}
                  />
                  <FormControl.Feedback />
                </FormGroup>

                <FormGroup
                  controlId='formContractCategory'
                >
                  <ControlLabel>Category</ControlLabel>
                  <Select.Async
                    name='form-field-name'
                    value={this.state.selectedCategory}
                    cache={false}
                    loadOptions={this.handleLodOptions}
                    onChange={(val) => {
                      let category_id
                      let html
                      let raw
                      let meta = []
                      let selectedCategory = {
                        value : ''
                      }

                      if (val) {
                        selectedCategory = val
                        category_id = val.id
                        html = val.html,
                        raw = val.raw
                      }

                      this.setState({
                        category_id,
                        selectedCategory,
                        html,
                        raw
                      })
                    }}
                  />
                </FormGroup>
              </form>
            </Col>
          </Row>
        </Grid>

        <br />
        <br />
        {/* {this.renderTemplate(this.state.category_id, this.state.html)} */}
        <EmployeeContract ref={(editor) => { this.editor = editor }}
          rawContentStateStr={this.state.raw} />
        <hr />
        <button className='btn btn-primary' onClick={() => {
          console.log(this.editor.getHTML())
          console.log(this.editor.getRaw(), JSON.stringify(this.editor.getRaw()))
        }}>Save</button>
      </div>
    )
  }
}

export default ContractNew
