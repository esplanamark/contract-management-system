import React, { Component } from 'react'
import {
  Button,
  ControlLabel,
  FormGroup,
  FormControl,
  HelpBlock,
  Popover,
  Tooltip,
  Modal,
  OverlayTrigger,
  Row,
  Grid,
  Col
} from 'react-bootstrap'
import {
  convertFromHTML,
  convertToRaw,
  ContentState,
  EditorState
} from 'draft-js'
import draftToHtml from 'draftjs-to-html' // eslint-disable-line import/no-extraneous-dependencies
import RichTextEditor from 'react-rte'
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

const rawContentState = { 'entityMap':{}, 'blocks':[{ 'key':'dfdaa', 'text':'asdasdsad', 'type':'unstyled', 'depth':0, 'inlineStyleRanges':[], 'entityRanges':[], 'data':{} }] }

class EmployeeContract extends Component {
  constructor (props) {
    super(props)

    this.state = {
      text: '',
      salary: '',
      employee_name: '',
      value: RichTextEditor.createEmptyValue(),
      editorState: undefined,
      html: undefined,
      rawContentStateStr: undefined,
      contentState: rawContentState
    }
  }

  componentWillReceiveProps (nextProps) {
    console.log('nextProps', nextProps)
    // if (this.state.html != nextProps.html) {
    //   const contentBlocks = convertFromHTML(nextProps.html)
    //   const contentState = ContentState.createFromBlockArray(contentBlocks)
    //   const editorState = EditorState.createWithContent(contentState)
    //
    //   this.setState({
    //     html: nextProps.html,
    //     editorState
    //   })
    // }

    if (this.state.rawContentStateStr != nextProps.rawContentStateStr) {
      const contentState = JSON.parse(nextProps.rawContentStateStr)

      console.log('contentState', contentState)
      this.setState({
        rawContentStateStr: nextProps.rawContentStateStr,
        contentState
      })
    }
  }

  onChange = (value) => {
    this.setState({ value })
    let editorState = this.state.value.getEditorState()
    let contentState = editorState.getCurrentContent()
    let rawContentState = convertToRaw(contentState)

    console.log('rawContentState', JSON.stringify(rawContentState))

    if (this.props.onChange) {
      // Send the changes up to the parent component as an HTML string.
      // This is here to demonstrate using `.toString()` but in a real app it
      // would be better to avoid generating a string on each change.
      this.props.onChange(
        value.toString('html')
      )
    }
  }

  getHTML = () => {
    let editorState = this.state.editorState
    let contentState = editorState.getCurrentContent()
    let rawContentState = convertToRaw(contentState)
    return draftToHtml(rawContentState)
  }

  getRaw = () => {
    let editorState = this.state.editorState
    let contentState = editorState.getCurrentContent()
    let rawContentState = convertToRaw(contentState)
    return rawContentState
  }

  onEditorStateChange = (editorState) => {
    this.setState({
      editorState
    })

    let contentState = editorState.getCurrentContent()
    let rawContentState = convertToRaw(contentState)

    if (this.props.onChange) {
      this.props.onChange(rawContentState)
    }
  }

  render () {
    const { editorState, contentState } = this.state
    return (
      <div>
        <Editor
          contentState={contentState}
          editorState={editorState}
          onEditorStateChange={this.onEditorStateChange}
        />
      </div>
    )
  }
}

export default EmployeeContract
