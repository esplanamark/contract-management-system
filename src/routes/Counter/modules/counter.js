import { takeLatest } from 'redux-saga'
import { take, put, select, call, fork } from 'redux-saga/effects'
import * as Api from './api'
import _ from 'lodash'
// ------------------------------------
// Constants
// ------------------------------------
const PAGINATION_DEF = {
  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

export const COUNTER_INCREMENT = 'COUNTER_INCREMENT'
export const COUNTER_DOUBLE = 'COUNTER_DOUBLE'

export const CONTRACT_CREATE_REQUESTED = 'CONTRACT_CREATE_REQUESTED'
export const CONTRACT_CREATE_SUCCEEDED = 'CONTRACT_CREATE_SUCCEEDED'
export const CONTRACT_CREATE_FAILED = 'CONTRACT_CREATE_FAILED'

export const CONTRACT_UPDATE_REQUESTED = 'CONTRACT_UPDATE_REQUESTED'
export const CONTRACT_UPDATE_SUCCEEDED = 'CONTRACT_UPDATE_SUCCEEDED'
export const CONTRACT_UPDATE_FAILED = 'CONTRACT_UPDATE_FAILED'

export const CONTRACT_DESTROY_REQUESTED = 'CONTRACT_DESTROY_REQUESTED'
export const CONTRACT_DESTROY_SUCCEEDED = 'CONTRACT_DESTROY_SUCCEEDED'
export const CONTRACT_DESTROY_FAILED = 'CONTRACT_DESTROY_FAILED'

export const CONTRACT_FETCH_REQUESTED = 'CONTRACT_FETCH_REQUESTED'
export const CONTRACT_FETCH_SUCCEEDED = 'CONTRACT_FETCH_SUCCEEDED'
export const CONTRACT_FETCH_FAILED = 'CONTRACT_FETCH_FAILED'

export const CONTRACT_FETCH_BY_ID_REQUESTED = 'CONTRACT_FETCH_BY_ID_REQUESTED'
export const CONTRACT_FETCH_BY_ID_SENDING = 'CONTRACT_FETCH_BY_ID_SENDING'
export const CONTRACT_FETCH_BY_ID_SUCCEEDED = 'CONTRACT_FETCH_BY_ID_SUCCEEDED'
export const CONTRACT_FETCH_BY_ID_FAILED = 'CONTRACT_FETCH_BY_ID_FAILED'

export const CONTRACT_SHOW_MODAL = 'CONTRACT_SHOW_MODAL'
export const CONTRACT_HIDE_MODAL = 'CONTRACT_HIDE_MODAL'

export const CONTRACT_SHOW_DELETE_MODAL = 'CONTRACT_SHOW_DELETE_MODAL'
export const CONTRACT_HIDE_DELETE_MODAL = 'CONTRACT_HIDE_DELETE_MODAL'

// ------------------------------------
// Actions
// ------------------------------------
export function increment (value = 1) {
  return {
    type    : COUNTER_INCREMENT,
    payload : value
  }
}

export function double () {
  return {
    type: COUNTER_DOUBLE
  }
}

export function showContractModal (contract) {
  return {
    type: CONTRACT_SHOW_MODAL,
    contract
  }
}

export function hideContractModal () {
  return {
    type: CONTRACT_HIDE_MODAL
  }
}

export function showContractDeleteModal (contract) {
  return {
    type: CONTRACT_SHOW_DELETE_MODAL,
    contract
  }
}

export function hideContractDeleteModal () {
  return {
    type: CONTRACT_HIDE_DELETE_MODAL
  }
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function fetchContractRequest (data) {
  return { type: CONTRACT_FETCH_REQUESTED, data }
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function fetchContractByIdRequest (contractId) {
  return { type: CONTRACT_FETCH_BY_ID_REQUESTED, contractId }
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function addContractRequest (data) {
  return { type: CONTRACT_CREATE_REQUESTED, data }
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function updateContractRequest (data) {
  return { type: CONTRACT_UPDATE_REQUESTED, data }
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function destroyContractRequest (data) {
  return { type: CONTRACT_DESTROY_REQUESTED, data }
}

export const actions = {
  increment,
  doubleAsync,
  fetchContractRequest,
  fetchContractByIdRequest,
  addContractRequest,
  updateContractRequest,
  destroyContractRequest
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [COUNTER_INCREMENT] : (state, action) => Object.assign({}, state, {
    counter: state.counter + action.payload
  }),
  [CONTRACT_FETCH_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    list: action.contracts,
    contractsTotalRedcord: action.recordsTotal,
    contractsRecordsFiltered: action.recordsFiltered,

    page: action.params.page,
    pageSize: action.params.pageSize,
    searchText: action.params.searchText,
    sortName: action.params.sortName,
    sortOrder: action.params.sortOrder
  }),
  [CONTRACT_FETCH_BY_ID_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    currentContract: action.contract,
    isCreateContractSending: false
  }),
  [CONTRACT_FETCH_BY_ID_SENDING] : (state, action) => Object.assign({}, state, {
    isFetchByIdSending: action.sending
  }),
  [CONTRACT_CREATE_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    isCreateContractSending: false,
    isCreateContractSuccess: true,
    isShowContractModal: false
  }),
  [CONTRACT_CREATE_REQUESTED] : (state, action) => Object.assign({}, state, {
    isCreateContractSending: true
  }),
  [CONTRACT_CREATE_FAILED] : (state, action) => Object.assign({}, state, {
    isCreateContractSending: false,
    createContractErrorMessage: action.message
  }),
  [CONTRACT_SHOW_MODAL] : (state, action) => Object.assign({}, state, {
    currentContract: action.contract,
    isShowContractModal: true
  }),
  [CONTRACT_HIDE_MODAL] : (state, action) => Object.assign({}, state, {
    isShowContractModal: false
  }),
  [CONTRACT_SHOW_DELETE_MODAL] : (state, action) => Object.assign({}, state, {
    currentContract: action.contract,
    isShowContractDeleteModal: true
  }),
  [CONTRACT_HIDE_DELETE_MODAL] : (state, action) => Object.assign({}, state, {
    isShowContractDeleteModal: false
  }),
  [CONTRACT_UPDATE_REQUESTED] : (state, action) => Object.assign({}, state, {
    isCreateContractSending: true
  }),
  [CONTRACT_UPDATE_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    isShowContractModal: false,
    isCreateContractSending: false
  }),
  [CONTRACT_UPDATE_FAILED] : (state, action) => Object.assign({}, state, {
    isCreateContractSending: false
  }),
  [CONTRACT_DESTROY_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    isShowContractDeleteModal: false
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  counter: 0,
  list: [],
  contractsTotalRedcord: 0,
  currentContract: {},
  isFetchByIdSending: false,
  isCreateContractSending: false,
  createContractErrorMessage: undefined,

  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined,

  isShowContractModal: false
}
export default function counterReducer (state = initialState, action) {
  console.log('counterReducer action', action)
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------
// worker Saga: will be fired on CONTRACT_FETCH_REQUESTED actions
function* fetchContract (action) {
  try {
    const { data: contracts, recordsTotal, recordsFiltered } = yield call(Api.fetchContracts, action.data)

    // const params = Object.assign({}, action.data || {}, ...PAGINATION_DEF)
    const params = _.defaults(action.data, PAGINATION_DEF)
    yield put({
      type: CONTRACT_FETCH_SUCCEEDED,
      params: params, // params from actions
      contracts,
      recordsTotal,
      recordsFiltered
    })
  } catch (e) {
    console.log('error', e)
    yield put({ type: CONTRACT_FETCH_FAILED, message: e.message })
  }
}

function* createContract (action) {
  try {
    const contracts = yield call(Api.createContracts, action.data)
    yield put({ type: CONTRACT_CREATE_SUCCEEDED, contracts: contracts })
    yield put({ type: CONTRACT_FETCH_REQUESTED, data: PAGINATION_DEF })
  } catch (e) {
    yield put({ type: CONTRACT_CREATE_FAILED, message: e.message })
  }
}

function* watchCreateContract () {
  // while (true) {
  //   let { payload } = yield take(CONTRACT_CREATE_REQUESTED)
  //   yield fork(createContract, payload.contract)
  // }
  yield takeLatest(CONTRACT_CREATE_REQUESTED, createContract)
}

function* fetchContractById (action) {
  // We send an action that tells Redux we're sending a request
  yield put({ type: CONTRACT_FETCH_BY_ID_SENDING, sending: true })

  try {
    const contract = yield call(Api.fetchContractById, action.contractId)
    yield put({ type: CONTRACT_FETCH_BY_ID_SUCCEEDED, contract: contract })
  } catch (e) {
    yield put({ type: CONTRACT_FETCH_BY_ID_FAILED, message: e.message })
  } finally {
    // When done, we tell Redux we're not in the middle of a request any more
    yield put({ type: CONTRACT_FETCH_BY_ID_SENDING, sending: false })
  }
}

function* watchFetchContractById () {
  yield takeLatest(CONTRACT_FETCH_BY_ID_REQUESTED, fetchContractById)
}

/*
  Alternatively you may use takeLatest.

  Does not allow concurrent fetches of user. If "CONTRACT_FETCH_REQUESTED" gets
  dispatched while a fetch is already pending, that pending fetch is cancelled
  and only the latest one will be run.
*/
export function* fetchContractAsync () {
  yield takeLatest(CONTRACT_FETCH_REQUESTED, fetchContract)
}

export function *doubleAsync () {
  while (true) {
    yield take(COUNTER_DOUBLE)
    const state = yield select()
    // yield asyncWait()
    yield put(increment(state.counter.counter))
  }
}

// Simulate an async call
const asyncWait = () => new Promise((resolve) => {
  setTimeout(() => resolve(), 200)
})

function* updateContract (action) {
  try {
    const contract = yield call(Api.updateContract, action.data)
    yield put({ type: CONTRACT_UPDATE_SUCCEEDED, contract })

    const state = yield select()
    const {
      page,
      pageSize,
      searchText,
      sortName,
      sortOrder
    } = state.counter

    const fetchParams = {
      page,
      pageSize,
      searchText,
      sortName,
      sortOrder
    }

    yield put({ type: CONTRACT_FETCH_REQUESTED, data: fetchParams })
  } catch (e) {
    yield put({ type: CONTRACT_UPDATE_FAILED, message: e.message })
  }
}

function* watchUpdateContract () {
  yield takeLatest(CONTRACT_UPDATE_REQUESTED, updateContract)
}

function* destroyCategory (action) {
  try {
    yield call(Api.destroyContract, action.data)
    yield put({ type:CONTRACT_DESTROY_SUCCEEDED })

    const state = yield select()
    const {
      page,
      pageSize,
      searchText,
      sortName,
      sortOrder,
      contractsRecordsFiltered,
      list
    } = state.counter

    // its just a computation if in last page and item is only one
    let computedPage = page
    if (list && list.length <= 1) {
      computedPage = computedPage - 1

      if (computedPage <= 0) {
        computedPage = 1
      }
    }

    const fetchParams = {
      page: computedPage,
      pageSize,
      searchText,
      sortName,
      sortOrder
    }
    yield put({ type: CONTRACT_FETCH_REQUESTED, data: fetchParams })
  } catch (e) {
    yield put({ type: CONTRACT_DESTROY_FAILED, message: e.message })
  }
}

function* watchDestroyContract () {
  yield takeLatest(CONTRACT_DESTROY_REQUESTED, destroyCategory)
}

export const sagas = [
  doubleAsync,
  fetchContractAsync,
  watchCreateContract,
  watchFetchContractById,
  watchUpdateContract,
  watchDestroyContract
]
