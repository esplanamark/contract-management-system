import axios from 'axios'

// import todoMapper from './todoMapper';

// import Todo from '../../containers/Todo';
import { URL } from '../../../consts/apiConsts'

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus (response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  }

  const error = new Error(response.statusText)
  error.response = response
  throw error
}

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON (response) {
  return response.data.data || response.data
}

export function fetchContracts (params) {
  return axios.get(`${URL}/contracts`, {
    params: params
  }).then(response => response.data)
}

export function fetchContractById (contractId) {
  return axios.get(`${URL}/contracts/${contractId}`).then(parseJSON)
}

export function createContracts (data) {
  let formData = new FormData()
  formData.append('name', data.name)
  formData.append('description', data.description)
  formData.append('category_id', data.category_id)
  formData.append('meta', JSON.stringify(data.meta))
  formData.append('html', data.html)
  formData.append('raw', data.raw)

  if (data.files) {
    let len = data.files.length
    for (let index = 0; index < len; ++index) {
      formData.append('files', data.files[index])
    }
  }

  return axios.post(`${URL}/contracts`, formData).then(parseJSON)
}

export function updateContract (data) {
  let formData = new FormData()
  formData.append('name', data.name)
  formData.append('description', data.description)
  formData.append('category_id', data.category_id)
  formData.append('meta', JSON.stringify(data.meta))
  formData.append('deleted_file_ids', JSON.stringify(data.deleted_file_ids))
  formData.append('html', data.html)
  formData.append('raw', data.raw)

  if (data.files) {
    let len = data.files.length
    for (let index = 0; index < len; ++index) {
      formData.append('files', data.files[index])
    }
  }

  return axios.put(`${URL}/contracts/${data.id}`, formData).then(parseJSON)
}

export function destroyContract (data) {
  return axios.delete(`${URL}/contracts/${data.id}`, data).then(parseJSON)
}
