import React from 'react'
import {
  Button,
  ControlLabel,
  FormGroup,
  FormControl,
  HelpBlock,
  Popover,
  Tooltip,
  Modal,
  OverlayTrigger
} from 'react-bootstrap'

export const ContractDeleteModal = (props) => (
  <Modal show={props.showModal} onHide={props.onHideModal}>
    <Modal.Header closeButton>
      <Modal.Title>Confirm Delete Contract</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      Are you sure you want to delete seleted contract?
      {/* { props.category ? props.category.id : 'no id' } */}
    </Modal.Body>
    <Modal.Footer>
      <Button onClick={props.onHideModal}>Close</Button>
      <Button
        bsStyle='primary'
        disabled={props.isDeleteContractSending}
        onClick={() => {
          props.handleSubmit(props.contract)
        }}>
          Ok
        </Button>
    </Modal.Footer>
  </Modal>
)

export default ContractDeleteModal
