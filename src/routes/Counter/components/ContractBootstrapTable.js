import React from 'react'
import { Link } from 'react-router'
import { Button } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

export default class ContractBootstrapTable extends React.Component {
  constructor (props) {
    super(props)
  }

  actionFormat = (cell, row) => {
    return (
      <div>
        <Link to={`/contracts/${row.id}`} className='btn btn-primary'>
          view
        </Link>
        {' '}
        <a className='btn btn-default' href={`/pages/contracts/${row.id}/pdf`} target='_blank'>
          pdf
        </a>
        {' '}
        <Button
          bsStyle='info'
          onClick={() => {
            this.props.onEdit(row)
          }}
        >
          edit
        </Button>
        {' '}
        <Button
          bsStyle='danger'
          onClick={() => {
            this.props.onDelete(row)
          }}
        >
          delete
        </Button>
      </div>
    )
  }

  render () {
    return (
      <BootstrapTable
        data={this.props.data}
        remote
        pagination
        search
        fetchInfo={{ dataTotalSize: this.props.dataTotalSize }}
        options={{ sizePerPage: this.props.sizePerPage,
          onPageChange: this.props.onPageChange,
          onSortChange: this.props.onSortChange,
          onSearchChange: this.props.onSearchChange,
          onSizePerPageList: this.props.onSizePerPageList,
          sizePerPageList: [ 5, 10 ],
          page: this.props.currentPage,
          paginationShowsTotal: true,
          defaultSearch: this.props.defaultSearch,
          defaultSortName: this.props.defaultSortName,
          defaultSortOrder: this.props.defaultSortOrder }}>
        <TableHeaderColumn dataField='id' isKey hidden>ID</TableHeaderColumn>
        <TableHeaderColumn dataField='name' dataSort>Contract Name</TableHeaderColumn>
        <TableHeaderColumn dataField='category_name' dataSort>Category</TableHeaderColumn>
        <TableHeaderColumn dataField='id' dataFormat={this.actionFormat}>--</TableHeaderColumn>
      </BootstrapTable>
    )
  }
}
