import _ from 'lodash'
import React, { Component } from 'react'
import {
  Button,
  ControlLabel,
  FormGroup,
  FormControl,
  HelpBlock,
  Popover,
  Tooltip,
  Modal,
  OverlayTrigger,
  Row,
  Grid,
  Col
} from 'react-bootstrap'
import {
  convertFromHTML,
  convertToRaw,
  ContentState,
  EditorState
} from 'draft-js'
import htmlToDraft from 'html-to-draftjs'
import draftToHtml from 'draftjs-to-html' // eslint-disable-line import/no-extraneous-dependencies
import RichTextEditor from 'react-rte'
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

const rawContentState = { 'entityMap':{}, 'blocks':[{ 'key':'dfdaa', 'text':'asdasdsad', 'type':'unstyled', 'depth':0, 'inlineStyleRanges':[], 'entityRanges':[], 'data':{} }] }

class ContractEditor extends Component {
  constructor (props) {
    super(props)

    this.state = {
      text: '',
      salary: '',
      employee_name: '',
      value: RichTextEditor.createEmptyValue(),
      editorState: undefined,
      html: undefined,
      rawContentStateStr: undefined,
      contentState: undefined
    }
  }

  componentWillReceiveProps (nextProps) {
    console.log('nextProps', nextProps)
    if (this.state.html != nextProps.html) {
      let editorState
      if (nextProps.html && nextProps.html != '') {
        const blocksFromHTML = htmlToDraft(nextProps.html)
        // const blocksFromHTML = convertFromHTML(nextProps.html)
        const contentState = ContentState.createFromBlockArray(
          blocksFromHTML.contentBlocks,
          blocksFromHTML.entityMap
        )
        // const contentBlocks = convertFromHTML(nextProps.html)
        // const contentState = ContentState.createFromBlockArray(contentBlocks)
        editorState = EditorState.createWithContent(contentState)
      } else {
        editorState = EditorState.createEmpty()
      }

      setTimeout(() => {
        this.setState({
          html: nextProps.html,
          editorState
        })
      })
    }

    // if (this.state.rawContentStateStr != nextProps.rawContentStateStr) {
    //   const { rawContentStateStr } = nextProps
    //   if (rawContentStateStr) {
    //     let contentState = {}
    //     if (_.isPlainObject(rawContentStateStr)) {
    //       contentState = rawContentStateStr
    //     } else {
    //       try {
    //         contentState = JSON.parse(rawContentStateStr)
    //       } catch (e) {}
    //     }
    //
    //     if (contentState.entityMap) {
    //       setTimeout(() => {
    //         this.setState({
    //           rawContentStateStr,
    //           contentState
    //         })
    //       })
    //     } else {
    //       let editorState = EditorState.createEmpty()
    //       setTimeout(() => {
    //         this.setState({
    //           editorState
    //         })
    //       })
    //     }
    //   } else {
    //     let editorState = EditorState.createEmpty()
    //     setTimeout(() => {
    //       this.setState({
    //         editorState
    //       })
    //     })
    //   }
    // }
  }

  onChange = (value) => {
    this.setState({ value })
    let editorState = this.state.value.getEditorState()
    let contentState = editorState.getCurrentContent()
    let rawContentState = convertToRaw(contentState)

    console.log('rawContentState', JSON.stringify(rawContentState))

    if (this.props.onChange) {
      // Send the changes up to the parent component as an HTML string.
      // This is here to demonstrate using `.toString()` but in a real app it
      // would be better to avoid generating a string on each change.
      this.props.onChange(
        value.toString('html')
      )
    }
  }

  getHTML = () => {
    let editorState = this.state.editorState
    if (editorState) {
      let contentState = editorState.getCurrentContent()
      let rawContentState = convertToRaw(contentState)
      return draftToHtml(rawContentState)
    }

    return ''
  }

  getRaw = () => {
    let editorState = this.state.editorState
    if (editorState) {
      let contentState = editorState.getCurrentContent()
      let rawContentState = convertToRaw(contentState)
      return rawContentState
    }

    return {}
  }

  onEditorStateChange = (editorState) => {
    this.setState({
      editorState
    })

    let contentState = editorState.getCurrentContent()
    let rawContentState = convertToRaw(contentState)

    if (this.props.onChange) {
      this.props.onChange(rawContentState)
    }
  }

  render () {
    const { editorState, contentState } = this.state
    return (
      <div>
        <Editor
          contentState={contentState}
          editorState={editorState}
          onEditorStateChange={this.onEditorStateChange}
        />
      </div>
    )
  }
}

export default ContractEditor
