import React, { Component } from 'react'
import { Link } from 'react-router'
import { Button, Popover, Tooltip, Modal, OverlayTrigger } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

import ContractModal from './ContractModal'
import ContractDeleteModal from './ContractDeleteModal'
import ContractBootstrapTable from './ContractBootstrapTable'

class ContractList extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isOpen: false,
      sortName: undefined,
      sortOrder: undefined,
      searchText: undefined,
      page: 1,
      pageSize: 10
    }

    // this.onShowPhotoShipe.bind(this)
    // this.handleClosePhotoSwipe.bind(this)
  }

  componentDidMount () {
    if (this.props.contracts.length <= 0) {
      this.props.fetchContract({
        page: 1
      })
    }
  }

  getModalState = () => {
    if (this.props.isCreateContractSending || this.props.createContractErrorMessage) {
      return true
    }

    return false
  }

  onShowPhotoShipe = () => {
    this.setState({
      isOpen: true,
      showModal: true
    })
  }

  sizePerPageListChange = (pageSize) => {
    const {
      sortName,
      sortOrder,
      searchText
    } = this.state

    const page = 1

    this.setState({
      pageSize,
      page
    })

    this.props.fetchContractRequest({
      pageSize,
      page,
      sortName,
      sortOrder,
      searchText
    })
  }

  onPageChange = (page, pageSize) => {
    const {
      sortName,
      sortOrder,
      searchText
    } = this.state

    this.setState({
      page: page
    })

    this.props.fetchContractRequest({
      pageSize,
      page,
      sortName,
      sortOrder,
      searchText
    })
  }

  onSortChange = (sortName, sortOrder) => {
    this.setState({
      sortName,
      sortOrder,
      page: 1
    })

    this.props.fetchContractRequest({
      sortName,
      sortOrder
    })
  }

  onSearchChange = (searchText, colInfos, multiColumnSearch) => {
    const text = searchText.trim()
    const {
      sortName,
      sortOrder
    } = this.state

    const page = 1
    this.setState({
      // sortName,
      // sortOrder,
      page,
      searchText: searchText
    })

    if (!text || text === '') {
      this.props.fetchContractRequest({
        page,
        sortName,
        sortOrder
      })

      return
    }

    this.props.fetchContractRequest({
      page,
      sortName,
      sortOrder,
      searchText
    })
  }

  handleEdit = (row) => {
    this.props.showContractModal(row)
  }

  handleDelete = (row) => {
    this.props.showContractDeleteModal(row)
  }

  handleCloseContractDeleteModal = () => {
    this.props.hideContractDeleteModal()
  }

  handleCloseContractModal = () => {
    this.props.hideContractModal()
  }

  handleSHowContractModal = () => {
    this.props.showContractModal({
      name: '',
      description: '',
      meta: [],
      category_id: ''
    })
  }

  render () {
    const { contracts } = this.props
    const props = this.props

    return (
      <div style={{ margin: '0 auto' }} >
        <Button
          bsStyle='primary'
          onClick={this.handleSHowContractModal}
        >
          Add new contract
        </Button>

        <ContractDeleteModal
          showModal={this.props.isShowContractDeleteModal}
          handleSubmit={this.props.destroyContract}
          contract={this.props.contract}

          onHideModal={this.handleCloseContractDeleteModal} />

        <ContractModal
          addContract={props.addContract}
          updateContract={props.updateContract}
          modalTitle='NodeJS'
          showModal={this.props.isShowContractModal}
          searchCategories={this.props.searchCategories}
          isCreateContractSending={this.props.isCreateContractSending}
          contract={this.props.contract}
          onCloseModal={this.handleCloseContractModal}
        />

        <ContractBootstrapTable
          data={contracts}
          pagination
          remote
          dataTotalSize={this.props.contractsRecordsFiltered}
          currentPage={this.props.page}
          sizePerPage={this.props.pageSize}
          onPageChange={this.onPageChange}
          onSortChange={this.onSortChange}
          onSearchChange={this.onSearchChange}
          onSizePerPageList={this.sizePerPageListChange}
          defaultSearch={this.props.searchText}
          defaultSortName={this.props.sortName}
          defaultSortOrder={this.props.sortOrder}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete} />
      </div>
    )
  }
}

ContractList.propTypes = {
  logout   : React.PropTypes.func.isRequired,
  addContract   : React.PropTypes.func.isRequired
}

export default ContractList
