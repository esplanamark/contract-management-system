import React, { Component } from 'react'
import { Link } from 'react-router'
import { PhotoSwipe } from 'react-photoswipe'
import Dropzone from 'react-dropzone'
import { Button,
  ControlLabel,
  FormGroup,
  FormControl,
  HelpBlock,
  Popover,
  Tooltip,
  Modal,
  OverlayTrigger
} from 'react-bootstrap'

import Select from 'react-select'
import ContractEditor from './ContractEditor'

class ContractModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isEdit: false,

      showModal: this.props.showModal || false,
      files: [],
      name: '',
      description: '',
      selectedCategory: {

      },
      contract: {},
      meta: [],
      deleted_file_ids: []
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.state.showModal != nextProps.showModal) {
      this.setState({
        showModal: nextProps.showModal
      })
    }

    if (this.state.contract != nextProps.contract) {
      const {
        id,
        name,
        description,
        meta,
        html,
        raw,
        files = []
      } = nextProps.contract

      this.setState({
        name,
        description,
        meta,
        files,
        html,
        raw,
        isEdit: id ? true : false
      })
    }
  }

  close = () => {
    // this.setState({ showModal: false })
    this.props.onCloseModal()
  }

  open = () => {
    this.setState({ showModal: true })
  }

  handleSubmit = () => {
    const {
      name,
      description,
      files = [],
      selectedCategory,
      isEdit,
      meta,
      deleted_file_ids
      // html,
      // raw
    } = this.state

    let html = this.editor.getHTML()
    let raw = JSON.stringify(this.editor.getRaw())

    if (isEdit) {
      this.props.updateContract({
        id: this.props.contract.id,
        name,
        description,
        files,
        meta,
        deleted_file_ids,
        html,
        raw
      })
    } else {
      this.props.addContract({
        category_id: selectedCategory.id,
        name,
        description,
        files,
        meta,
        html,
        raw
      })
    }
  }

  getValidationState = () => {
    const length = this.state.name.length
    if (length > 10) return 'success'
    else if (length > 5) return 'warning'
    else if (length > 0) return 'error'
  }

  handleChange = (e) => {
    this.setState({ name: e.target.value })
  }

  handleDescriptionChange = (e) => {
    this.setState({ description: e.target.value })
  }

  onDrop = (files) => {
    console.log('Received files: ', files)
    // let data = new FormData()
    // data.append('foo', 'bar')
    // data.append('file', files[0])

    this.setState({ files: [...this.state.files, ...files] })

    // this.setState(prevState => ({
    //   files: [prevState.files, ...files]
    // }))

    // axios.post('/upload/server', data)
  }

  onFileUploadCancel = (cancelledIndex) => {
    const myFiles = this.state.files.filter(function (file, i) {
      return i != cancelledIndex
    })

    this.setState({
      files: myFiles
    })
  }

  onFileUploadDelete = (cancelledIndex, fileId) => {
    const {
      files,
      deleted_file_ids
    } = this.state
    const myFiles = files.filter(function (file, i) {
      return i != cancelledIndex
    })

    this.setState({
      files: myFiles,
      deleted_file_ids: [...deleted_file_ids, fileId]
    })
  }

  handleMetaValueChange = (value, metaIndex) => {
    const { meta } = this.state

    this.setState({
      meta: meta.map((metaItem, i) => {
        if (i === metaIndex) {
          return Object.assign({}, metaItem, {
            value: value
          })
        }
        return metaItem
      })
    })
  }

  onDropzoneButtonlick = () => {
    this.dropzone.open()
  }

  renderFiles = (files) => {
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>Files</th>
            <th width='120px'>--</th>
          </tr>
        </thead>
        <tbody>
          {files.map((item, i) => {
            if (item.id) {
              return (
                <tr key={i}>
                  <td>
                    <a href={item.url} target='_blank'>
                      {
                        item.type === 'pdf' ?
                        item.name :
                        <img
                          src={item.url}
                          className='img-responsive'
                          width='240px'
                        />
                      }
                    </a>
                  </td>
                  <td>
                    <button
                      className='btn btn-danger btn-sm'
                      onClick={(ev) => {
                        ev.preventDefault()
                        this.onFileUploadDelete(i, item.id)
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              )
            }

            return (
              <tr key={i}>
                <td>
                  <a href={item.preview} target='_blank'>
                    {
                      item.type === 'application/pdf' ?
                      item.name :
                      <img
                        src={item.preview}
                        className='img-responsive'
                        width='240px'
                      />
                    }
                  </a>
                </td>
                <td>
                  <button
                    className='btn btn-warning btn-sm'
                    onClick={(ev) => {
                      ev.preventDefault()
                      this.onFileUploadCancel(i)
                    }}
                  >
                    Cancel
                  </button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    )
  }

  renderMeta = (meta) => {
    if (!meta) {
      return null
    }
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>Key</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          {meta.map((metaItem, i) => {
            return (
              <tr key={i}>
                <td>
                  {metaItem.label}
                </td>
                <td>
                  <FormControl
                    type='text'
                    value={metaItem.value}
                    placeholder='Enter text'
                    onChange={(e) => {
                      this.handleMetaValueChange(e.target.value, i)
                    }}
                  />
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    )
  }

  onCategoryChange = (val) => {
    let category_id, html, raw
    let meta = []
    let selectedCategory = {
      value : ''
    }

    console.log('onCategoryChange')
    console.log('val', val)

    if (val) {
      selectedCategory = val
      meta = val.meta
      raw = val.raw
      html = val.html
      category_id = val.id
    }

    this.setState({
      category_id,
      meta,
      html,
      raw,
      selectedCategory
    })
  }

  renderCategory = () => {
    if (this.state.isEdit) {
      return (
        <FormGroup
          controlId='formContractCategory'
        >
          <ControlLabel>Category</ControlLabel>
          <p>{this.props.contract.category_name}</p>

          {this.renderMeta(this.state.meta)}
        </FormGroup>
      )
    }

    return (
      <FormGroup
        controlId='formContractCategory'
      >
        <ControlLabel>Category</ControlLabel>
        <Select.Async
          name='form-field-name'
          value={this.state.selectedCategory}
          cache={false}
          loadOptions={this.props.searchCategories}
          onChange={this.onCategoryChange}
        />

        {this.renderMeta(this.state.meta)}
      </FormGroup>
    )
  }

  render () {
    const { html, raw } = this.state

    return (
      <div>
        <Modal
          show={this.props.showModal}
          onHide={this.close}
          bsSize='large'
        >
          <Modal.Header closeButton>
            <Modal.Title>Add Contract</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form>
              <FormGroup
                controlId='formContractName'
                // validationState={this.getValidationState()}
              >
                <ControlLabel>Name</ControlLabel>
                <FormControl
                  type='text'
                  value={this.state.name}
                  placeholder='Enter text'
                  onChange={this.handleChange}
                />
                <FormControl.Feedback />
                {/* <HelpBlock>Validation is based on string length.</HelpBlock> */}
              </FormGroup>

              <FormGroup
                controlId='formContractDescription'
                // validationState={this.getValidationState()}
              >
                <ControlLabel>Description</ControlLabel>
                <FormControl
                  componentClass='textarea'
                  placeholder='textarea'
                  value={this.state.description}
                  placeholder='Description'
                  onChange={this.handleDescriptionChange}
                />
                <FormControl.Feedback />
                {/* <HelpBlock>Validation is based on string length.</HelpBlock> */}
              </FormGroup>
              <FormGroup
                controlId='formContractFiles'
              >
                <ControlLabel>Images</ControlLabel>
                {' '}
                <Button
                  bsStyle='success'
                  bsSize='small'
                  onClick={this.onDropzoneButtonlick}>
                    Upload Image
                </Button>

                <Dropzone className='hide' onDrop={this.onDrop} ref={(node) => { this.dropzone = node }} />

                {this.renderFiles(this.state.files)}
              </FormGroup>
              {this.renderCategory()}
              <ContractEditor ref={(editor) => { this.editor = editor }}
                html={html}
                rawContentStateStr={raw}
              />
            </form>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
            <Button
              bsStyle='primary'
              disabled={this.props.isCreateContractSending}
              onClick={this.handleSubmit}>
              {this.props.isCreateContractSending ? 'Saving...' : 'Save'}
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

export default ContractModal
