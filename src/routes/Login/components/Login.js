import React, { Component } from 'react'
import {
  Button,
  Col,
  Checkbox,
  ControlLabel,
  Form,
  FormControl,
  FormGroup
} from 'react-bootstrap'

import DuckImage from 'assets/Duck.jpg'

class Login extends Component {
  constructor (props) {
    super(props)

    this.state = {
      username: '',
      password: ''
    }

    this._login = this._login.bind(this)
    this.handleUsernameChange = this.handleUsernameChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
  }

  handleUsernameChange (e) {
    this.setState({
      username: e.target.value
    })
  }

  handlePasswordChange (e) {
    this.setState({
      password: e.target.value
    })
  }

  _login () {
    const { username, password } = this.state
    this.props.loginRequest({ username, password })
  }

  render () {
    return (
      <div>
        <h3>Login</h3>
        <Form horizontal>
          <FormGroup controlId='formHorizontalEmail'>
            <Col componentClass={ControlLabel} sm={2}>
              Email
            </Col>
            <Col sm={10}>
              <FormControl type='email'
                placeholder='Email'
                value={this.state.username}
                onChange={this.handleUsernameChange} />
            </Col>
          </FormGroup>

          <FormGroup controlId='formHorizontalPassword'>
            <Col componentClass={ControlLabel} sm={2}>
              Password
            </Col>
            <Col sm={10}>
              <FormControl type='password' placeholder='Password'
                value={this.state.password}
                onChange={this.handlePasswordChange} />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button type='button'
                onClick={this._login}>
                Sign in
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    )
  }
}

Login.propTypes = {
  loginRequest   : React.PropTypes.func.isRequired
}

export default Login
