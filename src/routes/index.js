import { injectSagas } from '../store/sagas'

import myaxios from 'myaxios'
// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import SiteLayout from '../layouts/SiteLayout/SiteLayout'
import Home from './Home'
import * as authModule from 'modules/auth'

// import CounterRoute from './Counter'
// import LoginRoute from './Login'

/**
* Checks authentication status on route change
* @param  {object}   nextState The state we want to change into when we change routes
* @param  {function} replace Function provided by React Router to replace the location
*/
function checkAuth (nextState, replace, store) {
  let { auth, location } = store.getState()
  let { loggedIn } = auth

  // store.dispatch(clearError())

  console.log('loggedIn', loggedIn, 'location', location, 'nextState', nextState)
  // If the user is already logged in, forward them to the homepage
  if (!loggedIn) {
    if (nextState.location.pathname !== '/login') {
      replace('/login')
    }
  }

  if (loggedIn) {
    // if (nextState.location.state && nextState.location.pathname) {
    if (nextState.location.pathname == '/login') {
      replace('/')
    }
    // }
    // if (nextState.location.state && nextState.location.pathname) {
    //   replace(nextState.location.pathname)
    // } else {
    //   replace('/')
    // }
  }

  // Check if the path isn't dashboard. That way we can apply specific logic to
  // display/render the path we want to
  // if (nextState.location.pathname !== '/') {
  //   if (loggedIn) {
  //     if (nextState.location.state && nextState.location.pathname) {
  //       replace(nextState.location.pathname)
  //     } else {
  //       replace('/')
  //     }
  //   }
  // } else {
  //   // If the user is already logged in, forward them to the homepage
  //   if (!loggedIn) {
  //     if (nextState.location.state && nextState.location.pathname) {
  //       replace(nextState.location.pathname)
  //     } else {
  //       replace('/login')
  //     }
  //   }
  // }
}

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = (store) => ({
  path        : '/',
  // component   : CoreLayout,
  getComponent: (nextState, cb) => {
    require.ensure([], (require) => {
      const authSagas = authModule.sagas

      injectSagas(store, { key: 'auth', sagas: authSagas })

      const authState = store.getState().auth

      if (authState && authState.loggedIn) {
        // myaxios.defaults.headers.common['Authorization'] = 'Bearer ' + authState.token
        return cb(null, CoreLayout)
      }

      // myaxios.defaults.headers.common['Authorization'] = false
      cb(null, SiteLayout)
    })
  },
  // indexRoute  : Home,
  indexRoute: {
    path: '/home',
    onEnter: (nextState, replace) => {
      return replace('/contracts')
    }
  },
  onEnter     : (nextState, replace) => checkAuth(nextState, replace, store),
  onChange: (prevState, nextState, replace) => checkAuth(nextState, replace, store),
  // childRoutes : [
  //   CounterRoute(store),
  //   LoginRoute(store)
  // ],
  getChildRoutes (location, cb) {
    require.ensure([], (require) => {
      // const sagas = require('../modules/auth').sagas
      //
      // injectSagas(store, { key: 'auth', sagas })

      cb(null, [
        // Remove imports!
        require('./Category').default(store),
        require('./Counter').default(store),
        require('./Login').default(store)
      ])
    })
  }
})

/*  Note: childRoutes can be chunked or otherwise loaded programmatically
    using getChildRoutes with the following signature:

    getChildRoutes (location, cb) {
      require.ensure([], (require) => {
        cb(null, [
          // Remove imports!
          require('./Counter').default(store)
        ])
      })
    }

    However, this is not necessary for code-splitting! It simply provides
    an API for async route definitions. Your code splitting should occur
    inside the route `getComponent` function, since it is only invoked
    when the route exists and matches.
*/

export default createRoutes
