import React from 'react'
import {
  Button,
  ControlLabel,
  FormGroup,
  FormControl,
  HelpBlock,
  Popover,
  Tooltip,
  Modal,
  OverlayTrigger
} from 'react-bootstrap'

export const CategoryDeleteModal = (props) => (
  <Modal show={props.showModal} onHide={props.onHideModal}>
    <Modal.Header closeButton>
      <Modal.Title>Confirm Delete Category</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      Are you sure you want to delete seleted category?
      {/* { props.category ? props.category.id : 'no id' } */}
    </Modal.Body>
    <Modal.Footer>
      <Button onClick={props.onHideModal}>Close</Button>
      <Button
        bsStyle='primary'
        disabled={props.isDeleteCategorySending}
        onClick={() => {
          props.handleSubmit(props.category)
        }}>
          Ok
        </Button>
    </Modal.Footer>
  </Modal>
)

export default CategoryDeleteModal
