import React, { Component } from 'react'
import { Link } from 'react-router'
import { Button, Popover, Tooltip, Modal, OverlayTrigger } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

import CategoryModal from './CategoryModal'
import CategoryDeleteModal from './CategoryDeleteModal'
import CategoryBootstrapTable from './CategoryBootstrapTable'

class Category extends Component {
  constructor (props) {
    super(props)

    this.state = {
      sortName: undefined,
      sortOrder: undefined,
      searchText: undefined,
      page: 1,
      pageSize: 10
    }
  }

  componentDidMount() {
    this.props.fetchCategory({
      page: 1
    })
  }

  onSizePerPageListChange = (pageSize) => {
    const {
      sortName,
      sortOrder,
      searchText,
    } = this.state;

    const page = 1;

    this.setState({
      pageSize,
      page
    });

    this.props.fetchCategory({
      pageSize,
      page,
      sortName,
      sortOrder,
      searchText,
    })
  }

  onPageChange = (page, pageSize) => {
    const {
      sortName,
      sortOrder,
      searchText,
    } = this.state;

    this.setState({
      page: page
    });

    this.props.fetchCategory({
      pageSize,
      page,
      sortName,
      sortOrder,
      searchText,
    })
  }

  onSortChange = (sortName, sortOrder) => {
    this.setState({
      sortName,
      sortOrder,
      page: 1
    });

    this.props.fetchCategory({
      sortName,
      sortOrder
    })
  }

  onSearchChange = (searchText, colInfos, multiColumnSearch) => {
    const text = searchText.trim();
    const {
      sortName,
      sortOrder,
    } = this.state;

    const page = 1;
    this.setState({
      // sortName,
      // sortOrder,
      page,
      searchText: searchText
    });

    if (!text || text === '') {
      this.props.fetchCategory({
        page,
        sortName,
        sortOrder
      })

      return;
    }

    this.props.fetchCategory({
      page,
      sortName,
      sortOrder,
      searchText
    })
  }

  onEdit = (row) => {
    this.props.editCategory(row)
  }

  onDelete = (row) => {
    this.props.showDeleteModal(row)
  }

  render () {
    const { categories } = this.props;

    return (
      <div style={{ margin: '0 auto' }} >
        <CategoryDeleteModal
          showModal={this.props.isShowCategoryDeleteModal}
          onHideModal={this.props.hideDeleteModal}
          handleSubmit={this.props.destroyCategory}
          category={this.props.category}/>
        <CategoryModal
          onSubmitCategory={this.props.addCategory}
          onUpdateCategory={this.props.updateCategory}
          modalTitle="NodeJS"
          showModal={this.props.isShowCategoryModal}
          onShowModal={this.props.showModal}
          onHideModal={this.props.hideModal}
          category={this.props.category}
        />
        <CategoryBootstrapTable
          data={categories}
          pagination={ true }
          remote={ true }
          dataTotalSize={this.props.recordsFiltered}
          currentPage={this.state.page}
          sizePerPage={this.state.pageSize}
          onPageChange={this.onPageChange}
          onSortChange={this.onSortChange}
          onSearchChange={this.onSearchChange}
          onSizePerPageList={this.onSizePerPageListChange}
          onEdit={this.onEdit}
          onDelete={this.onDelete}
        />
      </div>
    )
  }
}

Category.propTypes = {
  addCategory   : React.PropTypes.func.isRequired
}

export default Category
