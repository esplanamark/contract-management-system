import React, { Component } from 'react'
import { Link } from 'react-router'
import { PhotoSwipe } from 'react-photoswipe'
import Dropzone from 'react-dropzone'
import {
  Button,
  ControlLabel,
  FormGroup,
  FormControl,
  HelpBlock,
  Popover,
  Tooltip,
  Modal,
  OverlayTrigger
} from 'react-bootstrap'
import {
  convertFromHTML,
  convertToRaw,
  ContentState,
  EditorState
} from 'draft-js'
import draftToHtml from 'draftjs-to-html' // eslint-disable-line import/no-extraneous-dependencies
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

const rawContentState = { 'entityMap':{}, 'blocks':[{ 'key':'1l42g', 'text':'Center', 'type':'unstyled', 'depth':0, 'inlineStyleRanges':[{ 'offset':0, 'length':6, 'style':'BOLD' }], 'entityRanges':[], 'data':{ 'text-align':'center' } }] }

class CategoryModal extends Component {
  constructor (props) {
    super(props)

    const defCategory = {
      name: '',
      description: '',
      meta: []
    }

    this.state = {
      editorState: undefined,
      contentState: {},
      showModal: this.props.showModal || false,
      category: this.props.category || defCategory
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.state.showModal != nextProps.showModal) {
      this.setState({
        showModal: nextProps.showModal
      })
    }

    if (this.state.category != nextProps.category) {
      const { category } = nextProps
      let contentState = {}
      if (category && category.raw) {
        try {
          contentState = JSON.parse(category.raw)
        } catch (e) {}

        this.setState({
          category
        }, () => {
          // we do this inorder to fixed issue
          // FIXME:: check issue 41
          // https://github.com/jpuri/react-draft-wysiwyg/issues/41
          setTimeout(() => {
            this.setState({
              contentState
            })
          })
        })
      }
    }
  }

  close = () => {
    this.setState({
      name: '',
      description: ''
    }, () => {
      this.props.onHideModal()
    })
    // this.setState({ showModal: false })
  }

  open = () => {
    this.props.onShowModal(true)
    // this.setState({ showModal: true })
  }

  handleMetaLabelChange = (value, metaIndex) => {
    this.setState({
      category: Object.assign({}, this.state.category, {
        meta: this.state.category.meta.map((meta, i) => {
          if (i == metaIndex) {
            return Object.assign({}, meta, {
              label: value
            })
          }

          return meta
        })
      })
    })
  }

  handleSubmit = () => {
    const { category } = this.state

    if (category.id) {
      this.props.onUpdateCategory(category)
    } else {
      this.props.onSubmitCategory(category)
    }
  }

  getValidationState = () => {
    // const length = this.state.category.name.length
    // if (length > 10) return 'success'
    // else if (length > 5) return 'warning'
    // else if (length > 0) return 'error'
  }

  handleChange = (e) => {
    this.setState({
      category: Object.assign({}, this.state.category, {
        name: e.target.value
      })
    })
    // this.setState({ name: e.target.value })
  }

  handleDescriptionChange = (e) => {
    this.setState({
      category: Object.assign({}, this.state.category, {
        description: e.target.value
      })
    })
    // this.setState({ description: e.target.value })
  }

  newMeta = () => {
    const newMeta = {
      label: '',
      key: ''
    }

    const oldMeta = this.state.category.meta || []

    this.setState({
      category: Object.assign({}, this.state.category, {
        meta: [...oldMeta, newMeta]
      })
    })
  }

  deleteMeta = (cancelledIndex) => {
    const meta = this.state.category.meta.filter(function (metaItem, i) {
      return i != cancelledIndex
    })

    this.setState({
      category: Object.assign({}, this.state.category, {
        meta: meta
      })
    })
  }

  // onContentStateChange = (contentState) => {
  //   this.setState({
  //     contentState
  //   })
  // }

  onEditorStateChange = (editorState) => {
    const contentState = editorState.getCurrentContent()
    const rawContentState = convertToRaw(contentState)
    const raw = JSON.stringify(rawContentState)
    const html = draftToHtml(rawContentState)

    this.setState({
      editorState,
      category: Object.assign({}, this.state.category, {
        html,
        raw
      })
    })
  }

  renderMeta = (meta) => {
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>Meta</th>
            <th width='120px'>--</th>
          </tr>
        </thead>
        <tbody>
          {meta.map((metaItem, i) => {
            return (
              <tr key={i}>
                <td>
                  <FormControl
                    type='text'
                    value={metaItem.label}
                    placeholder='Enter text'
                    onChange={(e) => {
                      this.handleMetaLabelChange(e.target.value, i)
                    }}
                  />
                </td>
                <td>
                  <Button
                    bsStyle='danger'
                    onClick={() => {
                      this.deleteMeta(i)
                    }}
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    )
  }

  renderEditor = () => {
    const { contentState, editorState } = this.state

    return (
      <Editor
        placeholder='Enter text...'
        contentState={contentState}
        editorState={editorState}
        onEditorStateChange={this.onEditorStateChange}
        onContentStateChange={this.onContentStateChange}
      />
    )
  }
  render () {
    const { category, contentState, editorState } = this.state

    if (!category) {
      return
    }

    return (
      <div>
        <Button
          bsStyle='primary'
          onClick={this.open}
        >
          Add new category
        </Button>

        <Modal show={this.state.showModal} onHide={this.close} bsSize='large'>
          <Modal.Header closeButton>
            <Modal.Title>Add Category</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form>
              <FormGroup
                controlId='formContractName'
                validationState={this.getValidationState()}
              >
                <ControlLabel>Name</ControlLabel>
                <FormControl
                  type='text'
                  value={this.state.category.name}
                  placeholder='Enter text'
                  onChange={this.handleChange}
                />
                <FormControl.Feedback />
                {/* <HelpBlock>Validation is based on string length.</HelpBlock> */}
              </FormGroup>

              <FormGroup
                controlId='formContractDescription'
                // validationState={this.getValidationState()}
              >
                <ControlLabel>Description</ControlLabel>
                <FormControl
                  componentClass='textarea'
                  placeholder='textarea'
                  value={this.state.category.description}
                  placeholder='Description'
                  onChange={this.handleDescriptionChange}
                />
                <FormControl.Feedback />
                {/* <HelpBlock>Validation is based on string length.</HelpBlock> */}
              </FormGroup>

              {this.renderMeta(category.meta || [])}
              <Button bsSize='small' onClick={this.newMeta}>New Meta</Button>
              <br />
              <br />
              {this.renderEditor()}
            </form>

          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
            <Button
              bsStyle='primary'
              disabled={this.props.isCreateContractSending}
              onClick={this.handleSubmit}>
              {this.props.isCreateContractSending ? 'Saving...' : 'Save'}
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

export default CategoryModal
