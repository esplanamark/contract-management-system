import React from 'react';
import { Link } from 'react-router'
import { Button } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

export default class CategoryBootstrapTable extends React.Component {
  constructor(props) {
    super(props);
  }

  actionFormat = (cell, row) => {
    return (
      <div>
        <Button
          bsStyle='info'
          onClick={() => {
            this.props.onEdit(row)
          }}
        >
          edit
        </Button>
        {' '}
        <Button
          bsStyle='danger'
          onClick={() => {
            this.props.onDelete(row)
          }}
        >
          delete
        </Button>
      </div>
    )
  }

  render() {
    return (
      <BootstrapTable
        data={ this.props.data }
        remote={ true }
        pagination={ true }
        search = { true }
        fetchInfo={ { dataTotalSize: this.props.dataTotalSize } }
        options={ { sizePerPage: this.props.sizePerPage,
                    onPageChange: this.props.onPageChange,
                    onSortChange: this.props.onSortChange,
                    onSearchChange: this.props.onSearchChange,
                    onSizePerPageList: this.props.onSizePerPageList,
                    sizePerPageList: [ 5, 10 ],
                    page: this.props.currentPage,
                    paginationShowsTotal: true } }>
        <TableHeaderColumn dataField='id' isKey={ true }>ID</TableHeaderColumn>
        <TableHeaderColumn dataField='name' dataSort={ true }>Category Name</TableHeaderColumn>
        <TableHeaderColumn dataField='id' dataFormat={ this.actionFormat }>--</TableHeaderColumn>
      </BootstrapTable>
    );
  }
}
