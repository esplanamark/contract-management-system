import { takeLatest } from 'redux-saga'
import { take, put, select, call, fork } from 'redux-saga/effects'
import * as Api from './api'
// ------------------------------------
// Constants
// ------------------------------------

export const CATEGORY_CREATE_REQUESTED = 'CATEGORY_CREATE_REQUESTED'
export const CATEGORY_CREATE_SUCCEEDED = 'CATEGORY_CREATE_SUCCEEDED'
export const CATEGORY_CREATE_FAILED = 'CATEGORY_CREATE_FAILED'

export const CATEGORY_UPDATE_REQUESTED = 'CATEGORY_UPDATE_REQUESTED'
export const CATEGORY_UPDATE_SUCCEEDED = 'CATEGORY_UPDATE_SUCCEEDED'
export const CATEGORY_UPDATE_FAILED = 'CATEGORY_UPDATE_FAILED'

export const CATEGORY_FETCH_REQUESTED = 'CATEGORY_FETCH_REQUESTED'
export const CATEGORY_FETCH_SUCCEEDED = 'CATEGORY_FETCH_SUCCEEDED'
export const CATEGORY_FETCH_FAILED = 'CATEGORY_FETCH_FAILED'

export const CATEGORY_DESTROY_REQUESTED = 'CATEGORY_DESTROY_REQUESTED'
export const CATEGORY_DESTROY_SUCCEEDED = 'CATEGORY_DESTROY_SUCCEEDED'
export const CATEGORY_DESTROY_FAILED = 'CATEGORY_DESTROY_FAILED'

export const CATEGORY_FETCH_BY_ID_REQUESTED = 'CATEGORY_FETCH_BY_ID_REQUESTED'
export const CATEGORY_FETCH_BY_ID_SENDING = 'CATEGORY_FETCH_BY_ID_SENDING'
export const CATEGORY_FETCH_BY_ID_SUCCEEDED = 'CATEGORY_FETCH_BY_ID_SUCCEEDED'
export const CATEGORY_FETCH_BY_ID_FAILED = 'CATEGORY_FETCH_BY_ID_FAILED'

export const CATEGORY_TOGGLE_MODAL = 'CATEGORY_TOGGLE_MODAL'
export const CATEGORY_EDIT_MODAL = 'CATEGORY_EDIT_MODAL'
export const CATEGORY_DELETE_MODAL = 'CATEGORY_DELETE_MODAL'
export const CATEGORY_TOGGLE_DELETE_MODAL = 'CATEGORY_TOGGLE_DELETE_MODAL'

const PAGINATION_DEF = {
  page: 1,
  pageSize: 10,
  searchText: '',
  sortName: undefined,
  sortOrder: undefined
}

const DEF_CATEGORY = {
  name: '',
  description: '',
  meta: []
}

// ------------------------------------
// Actions
// ------------------------------------

export function showModal (isAdd) {
  return { type: CATEGORY_TOGGLE_MODAL, isShow: true, isAdd }
}

export function hideModal () {
  return { type: CATEGORY_TOGGLE_MODAL, isShow: false }
}

export function showDeleteModal (category) {
  return { type: CATEGORY_DELETE_MODAL, category }
}

export function hideDeleteModal () {
  return { type: CATEGORY_TOGGLE_DELETE_MODAL, isShow: false }
}

export function editCategory (category) {
  return { type: CATEGORY_EDIT_MODAL, category }
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function fetchCategoryRequest (data) {
  return { type: CATEGORY_FETCH_REQUESTED, data }
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function fetchCategoryByIdRequest (categoryId) {
  return { type: CATEGORY_FETCH_BY_ID_REQUESTED, categoryId }
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function addCategoryRequest (data) {
  return { type: CATEGORY_CREATE_REQUESTED, data }
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function updateCategoryRequest (data) {
  return { type: CATEGORY_UPDATE_REQUESTED, data }
}

/**
 * Tells the app we want to log in a user
 * @param  {object} data  The data we're sending for log in
 */
export function destroyCategoryRequest (data) {
  return { type: CATEGORY_DESTROY_REQUESTED, data }
}

export const actions = {
  fetchCategoryRequest,
  fetchCategoryByIdRequest,
  addCategoryRequest,

  showModal,
  hideModal,
  editCategory
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [CATEGORY_FETCH_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    categories: action.categories,
    categoriesTotalRedcord: action.recordsTotal,
    categoriesRecordsFiltered: action.recordsFiltered,
    page: action.page
  }),
  [CATEGORY_FETCH_BY_ID_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    currentCategory: action.category,
    isCreateCategorySending: false
  }),
  [CATEGORY_FETCH_BY_ID_SENDING] : (state, action) => Object.assign({}, state, {
    isFetchByIdSending: action.sending
  }),
  [CATEGORY_CREATE_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    isCreateCategorySending: false,
    isCreateCategorySuccess: true,
    isShowCategoryModal: false
  }),
  [CATEGORY_CREATE_REQUESTED] : (state, action) => Object.assign({}, state, {
    isCreateCategorySending: true
  }),
  [CATEGORY_CREATE_FAILED] : (state, action) => Object.assign({}, state, {
    isCreateCategorySending: false,
    createCategoryErrorMessage: action.message
  }),
  [CATEGORY_UPDATE_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    isShowCategoryModal: false,
    categories: state.categories.map((item) => {
      if (item.id === action.category.id) {
        return Object.assign({}, item, {
          name: action.category.name,
          description: action.category.description,
          raw: action.category.raw,
          meta: action.category.meta
        })
      }
      return item
    })
  }),

  [CATEGORY_TOGGLE_MODAL] : (state, action) => Object.assign({}, state, {
    isShowCategoryModal: action.isShow,
    currentCategory: action.isAdd ? DEF_CATEGORY : action.category
  }),
  [CATEGORY_TOGGLE_DELETE_MODAL] : (state, action) => Object.assign({}, state, {
    isShowCategoryDeleteModal: action.isShow
  }),
  [CATEGORY_DELETE_MODAL] : (state, action) => Object.assign({}, state, {
    isShowCategoryDeleteModal: true,
    currentCategory: action.category
  }),
  [CATEGORY_EDIT_MODAL] : (state, action) => Object.assign({}, state, {
    isShowCategoryModal: true,
    currentCategory: action.category
  }),
  [CATEGORY_DESTROY_SUCCEEDED] : (state, action) => Object.assign({}, state, {
    isShowCategoryDeleteModal: false
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  categories: [],
  currentCategory: {},
  categoriesTotalRedcord: 0,
  categoriesRecordsFiltered: 0,
  isFetchByIdSending: false,
  isCreateCategorySending: false,
  createCategoryErrorMessage: undefined,
  isShowCategoryModal: false,
  isShowCategoryDeleteModal: false
}
export default function categoryReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

// ------------------------------------
// Sagas
// ------------------------------------
// worker Saga: will be fired on CATEGORY_FETCH_REQUESTED actions
function* fetchCategory (action) {
  try {
    const { data: categories, recordsTotal, recordsFiltered } = yield call(Api.fetchCategories, action.data)
    yield put({
      type: CATEGORY_FETCH_SUCCEEDED,
      categories,
      recordsTotal,
      recordsFiltered
    })
  } catch (e) {
    yield put({ type: CATEGORY_FETCH_FAILED, message: e.message })
  }
}

function* createCategory (action) {
  try {
    const categories = yield call(Api.createCategories, action.data)
    yield put({ type: CATEGORY_CREATE_SUCCEEDED, categories: categories })
    yield put({ type: CATEGORY_FETCH_REQUESTED, data: PAGINATION_DEF })
  } catch (e) {
    yield put({ type: CATEGORY_CREATE_FAILED, message: e.message })
  }
}

function* watchCreateCategory () {
  yield takeLatest(CATEGORY_CREATE_REQUESTED, createCategory)
}

function* fetchCategoryById (action) {
  // We send an action that tells Redux we're sending a request
  yield put({ type: CATEGORY_FETCH_BY_ID_SENDING, sending: true })

  try {
    const category = yield call(Api.fetchCategoryById, action.categoryId)
    yield put({ type: CATEGORY_FETCH_BY_ID_SUCCEEDED, category: category })
  } catch (e) {
    yield put({ type: CATEGORY_FETCH_BY_ID_FAILED, message: e.message })
  } finally {
    // When done, we tell Redux we're not in the middle of a request any more
    yield put({ type: CATEGORY_FETCH_BY_ID_SENDING, sending: false })
  }
}

function* watchFetchCategoryById () {
  yield takeLatest(CATEGORY_FETCH_BY_ID_REQUESTED, fetchCategoryById)
}

/*
  Alternatively you may use takeLatest.

  Does not allow concurrent fetches of user. If "CATEGORY_FETCH_REQUESTED" gets
  dispatched while a fetch is already pending, that pending fetch is cancelled
  and only the latest one will be run.
*/
export function* watchFetchCategory () {
  yield takeLatest(CATEGORY_FETCH_REQUESTED, fetchCategory)
}

function* updateCategory (action) {
  try {
    const category = yield call(Api.updateCategory, action.data)
    yield put({ type: CATEGORY_UPDATE_SUCCEEDED, category })
  } catch (e) {
    yield put({ type: CATEGORY_UPDATE_FAILED, message: e.message })
  }
}

function* watchUpdateCategory () {
  yield takeLatest(CATEGORY_UPDATE_REQUESTED, updateCategory)
}

function* destroyCategory (action) {
  try {
    yield call(Api.destroyCategory, action.data)
    yield put({ type: CATEGORY_DESTROY_SUCCEEDED })
    yield put({ type: CATEGORY_FETCH_REQUESTED })
  } catch (e) {
    yield put({ type: CATEGORY_DESTROY_FAILED, message: e.message })
  }
}

function* watchDestroyCategory () {
  yield takeLatest(CATEGORY_DESTROY_REQUESTED, destroyCategory)
}

export const sagas = [
  watchFetchCategory,
  watchCreateCategory,
  watchFetchCategoryById,
  watchUpdateCategory,
  watchDestroyCategory
]
