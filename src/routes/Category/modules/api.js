import axios from 'axios'

// import todoMapper from './todoMapper';

// import Todo from '../../containers/Todo';
import { URL } from '../../../consts/apiConsts'

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus (response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  }

  const error = new Error(response.statusText)
  error.response = response
  throw error
}

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON (response) {
  return response.data.data || response.data
}

export function fetchCategories (params) {
  return axios.get(`${URL}/categories`, {
    params: params
  }).then(response => response.data)
}

export function fetchCategoryById (categoryId) {
  return axios.get(`${URL}/categories/${categoryId}`).then(parseJSON)
}

export function createCategories (data) {
  return axios.post(`${URL}/categories`, data).then(parseJSON)
}

export function updateCategory (data) {
  return axios.put(`${URL}/categories/${data.id}`, data).then(parseJSON)
}

export function destroyCategory (data) {
  return axios.delete(`${URL}/categories/${data.id}`, data).then(parseJSON)
}

export function autoCompleteCategories (inputText) {
  return fetchCategories({
    searchText: inputText
  }).then((categories) => {
    const formattedCategories = categories.data.map((category) => {
      return Object.assign({}, category, {
        value: category.id,
        label: category.name
      })
    })

    return {
      options: formattedCategories
    }
  })
}
