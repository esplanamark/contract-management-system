import { injectReducer } from '../../store/reducers'
import { injectSagas } from '../../store/sagas'

export default (store) => ({
  path : 'category',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Category = require('./containers/CategoryContainer').default
      const reducer = require('./modules/category').default
      const sagas = require('./modules/category').sagas

      /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'category', reducer })
      injectSagas(store, { key: 'category', sagas })

      /*  Return getComponent   */
      cb(null, Category)

    /* Webpack named bundle   */
    }, 'category')
  },
  // getChildRoutes (location, next) {
  //   require.ensure([], (require) => {
  //     next(null, [
  //       require('./routes/ContractDetail').default(store)
  //     ])
  //   })
  // }
})
