import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  addCategoryRequest,
  updateCategoryRequest,
  fetchCategoryRequest,
  destroyCategoryRequest,
  showModal,
  hideModal,
  editCategory,
  showDeleteModal,
  hideDeleteModal
} from '../modules/category'

import Category from '../components/Category'
/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the counter:   */

class CategoryContainer extends Component {
  render () {
    let { children, params } = this.props

    let content
    if (children) {
      content = this.props.children
    } else {
      content = <Category {...this.props} />
    }

    return (
      <div>
        <h1>Category</h1>
        <hr />
        {content}
      </div>
    )
  }
}

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around increment; the component doesn't care   */

const mapDispatchToProps = {
  addCategory: (...args) => addCategoryRequest(...args),
  updateCategory: (...args) => updateCategoryRequest(...args),
  fetchCategory: (...args) => fetchCategoryRequest(...args),
  destroyCategory: (...args) => destroyCategoryRequest(...args),
  showModal,
  hideModal,
  editCategory,
  showDeleteModal,
  hideDeleteModal
}

const mapStateToProps = (state) => ({
  category : state.category.currentCategory,
  categories: state.category.categories,
  recordsFiltered: state.category.categoriesRecordsFiltered,
  isShowCategoryModal: state.category.isShowCategoryModal,
  isShowCategoryDeleteModal: state.category.isShowCategoryDeleteModal,
})

/*  Note: mapStateToProps is where you should use `reselect` to create selectors, ie:

    import { createSelector } from 'reselect'
    const counter = (state) => state.counter
    const tripleCount = createSelector(counter, (count) => count * 3)
    const mapStateToProps = (state) => ({
      counter: tripleCount(state)
    })

    Selectors can compute derived data, allowing Redux to store the minimal possible state.
    Selectors are efficient. A selector is not recomputed unless one of its arguments change.
    Selectors are composable. They can be used as input to other selectors.
    https://github.com/reactjs/reselect    */

export default connect(mapStateToProps, mapDispatchToProps)(CategoryContainer)
