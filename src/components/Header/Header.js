import React from 'react'
import { IndexLink, Link } from 'react-router'
import { LinkContainer, IndexLinkContainer } from 'react-router-bootstrap'
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap'
import './Header.scss'

const getProfileName = (profile) => {
  if (profile) {
    return profile.name
  }

  return ''
}
export const Header = (props) => (
  <Navbar>
    <Navbar.Header>
      <IndexLinkContainer to='/'>
        <Navbar.Brand>
          <IndexLink to='/'>Contract Management</IndexLink>
        </Navbar.Brand>
      </IndexLinkContainer>
    </Navbar.Header>
    <Nav>
      <LinkContainer to='/contracts'>
        <NavItem>Contracts</NavItem>
      </LinkContainer>
      <LinkContainer to='/category'>
        <NavItem>Category</NavItem>
      </LinkContainer>
    </Nav>
    <Nav pullRight>
      <NavDropdown eventKey={3} title={getProfileName(props.auth.user)} id='basic-nav-dropdown'>
        <MenuItem eventKey={3.1}>Profile</MenuItem>
        <MenuItem divider />
        <MenuItem eventKey={3.3} onClick={props.logout}>Logout</MenuItem>
      </NavDropdown>
    </Nav>
  </Navbar>
)

export default Header
