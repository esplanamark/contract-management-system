const path = require('path')
const Promise = require('bluebird')
const pdf2img = require('pdf2img')
const PDFJS = require('pdfjs-dist')
const _ = require('lodash')
const fs = require('fs')
const gm = require('gm').subClass({ imageMagick: true })

PDFJS.PDFJS.verbosity = 0

// HACK few hacks to let PDF.js be loaded not as a module in global space.
require('./domstubs.js')
const constants = require('../constants')

const ADMIN_URL = '/adminx'

exports.adminUrl = (url) => path.join(ADMIN_URL, url)
exports.ADMIN_URL = ADMIN_URL

exports.apiResponse = (apiData, status = 200) => {
  if (apiData && apiData.data) {
    return Object.assign({}, apiData, {
      status
    })
  } else {
    return {
      data: apiData,
      status
    }
  }
}

exports.addFilenameSuffix = (filePath, str) => {
  return filePath.replace(/(\.[\w\d_-]+)$/i, str + '$1')
}

exports.getFileType = (mimetype) => {
  if (constants.MIMETYPE.image.indexOf(mimetype) >= 0) {
    return 'image'
  }

  if (constants.MIMETYPE.audio.indexOf(mimetype) >= 0) {
    return 'audio'
  }

  if (constants.MIMETYPE.video.indexOf(mimetype) >= 0) {
    return 'video'
  }

  if (constants.MIMETYPE.pdf.indexOf(mimetype) >= 0) {
    return 'pdf'
  }
}

exports.extractImagesFromPDF = (pdfPath, outputdir, options = {}) => {
  const PDFToPng = (input, output, opts) => {
    return new Promise(function (resolve, reject) {
      gm(input).density(opts.width, opts.height).write(output, function (err) {
        if (err) {
          return reject(err)
        }
        resolve()
      })
    })
  }

  return new Promise(function (resolve, reject) {
    const pdfData = new Uint8Array(fs.readFileSync(pdfPath))
    PDFJS.getDocument(pdfData).then(function (pdfDocument) {
      const numPages = pdfDocument.numPages
      // const numPages = 2
      console.log('Number of Pages: ' + numPages)

      let pages = []
      for (var i = 1; i <= numPages; i++) {
        pages.push({
          num: i
        })
      }
      const filename = getFileNameFromPath(pdfPath)

      return Promise.mapSeries(pages, (pageItem) => {
        return pdfDocument.getPage(pageItem.num).then(function (page) {
          const viewport = page.getViewport(1.0)
          const SVGfileName = filename + '_' + pageItem.num + '_.svg'
          const PNGfileName = filename + '_' + pageItem.num + '_.png'
          const SVGfilePath = outputdir + SVGfileName
          const PNGfilePath = outputdir + PNGfileName

          console.log('# Page ' + pageItem.num, 'PDFToPng')
          return PDFToPng(pdfPath + '[' + (pageItem.num - 1) + ']', PNGfilePath, {
            width: viewport.width,
            height: viewport.height
          }).then(() => {
            return {
              name: PNGfileName,
              path: PNGfilePath,
              svg: SVGfilePath
            }
          })
        })
      }).then((images) => {
        resolve(images)
      })
    })
  })
}

// Get filename from the path

function getFileNameFromPath (path) {
  var index = path.lastIndexOf('/')
  var extIndex = path.lastIndexOf('.')
  return path.substring(index, extIndex)
}
