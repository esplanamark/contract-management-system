const createError = require('http-errors')
const apiErrorHandler = require('api-error-handler')
const Promise = require('bluebird')
const jwt = require('jsonwebtoken')
const passport = require('passport')
const BearerStrategy = require('passport-http-bearer').Strategy

const apiRoutes = require('../routes/api')

// Middlewares
const upload = require('./upload')
const authStrategies = require('./auth-strategies')
// const jwtMiddleware = require('./jwtMiddleware')

const isBearerAutorizationHeader = (req) => {
  let parts,
    scheme,
    credentials

  if (req.headers && req.headers.authorization) {
    parts = req.headers.authorization.split(' ')
  } else if (req.query && req.query.access_token) {
    return true
  } else {
    return false
  }

  if (parts.length === 2) {
    scheme = parts[0]
    credentials = parts[1]
    if (/^Bearer$/i.test(scheme)) {
      return true
    }
  }
  return false
}

const authenticateUser = (req, res, next) => {
  return passport.authenticate('bearer', { session: false, failWithError: false },
    function authenticate (err, user, info) {
      if (err) {
        return next(err) // will generate a 500 error
      }

      if (user) {
        req.authInfo = info
        req.user = user
        return next(null, user, info)
      } else if (isBearerAutorizationHeader(req)) {
        return next(createError(401, 'Access denied.'))
      }
      return next(createError(401, 'Access denied.'))
    }
  )(req, res, next)
}

const requiresAuthorizedUser = (req, res, next) => {
  if (req.user && req.user.id) {
    return next()
  } else {
    return next(createError(401, 'Access denied.'))
  }
}

const middleware = {
  upload: upload.uploadFile,
  // jwtMiddleware: jwtMiddleware,
  api: {
    authenticateUser: authenticateUser,
    requiresAuthorizedUser: requiresAuthorizedUser,
    errorHandler: apiErrorHandler
  }
}

const setupMiddleware = (app) => {
  passport.use(new BearerStrategy(authStrategies.bearerStrategy))

  app.use('/api', apiRoutes(middleware))
}
module.exports.middleware = middleware
module.exports = setupMiddleware
