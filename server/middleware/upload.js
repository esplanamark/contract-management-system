const multer = require('multer')
const os = require('os')
// const mime = require('mime')
const mime = require('mime-types')
const crypto = require('crypto')

const tmpdir = os.tmpdir
const multerStorage = multer.diskStorage({
  destination: tmpdir(),
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype))
    })
  }
})

const uploadFile = multer({
  storage: multerStorage,
  fileFilter: function (req, file, cb) {
    cb(null, true)
    // if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
    //   return cb(new Error('Only image files are allowed!'));
    // }
    // cb(null, true);
  }
})

module.exports = {
  uploadFile: uploadFile
}
