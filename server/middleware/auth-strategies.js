const Promise = require('bluebird')
const jwt = require('jsonwebtoken')

/**
 * BearerStrategy
 *
 * This strategy is used to authenticate users based on an access token (aka a
 * bearer token).  The user must have previously authorized a client
 * application, which is issued an access token to make requests on behalf of
 * the authorizing user.
 */
const bearerStrategy = (token, done) => {
  if (token) {
    jwt.verify(token, 'itssecret_', function (err, decoded) {
      if (err) {
        return done(null, false)
      } else {
        let info = { scope: '*' }
        return done(null, decoded, info)
      }
    })
  } else {
    return done(null, false)
  }
}

const strategies = {
  bearerStrategy
}

module.exports = strategies
