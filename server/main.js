const express = require('express')
const debug = require('debug')('app:server')
const webpack = require('webpack')
const webpackConfig = require('../build/webpack.config')
const config = require('../config')
const compress = require('compression')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const exphbs = require('express-handlebars')
const Promise = require('bluebird')
const path = require('path')
const url = require('url')
const fs = require('fs')
const expressValidator = require('express-validator')

const constants = require('./constants')
const helpers = require('./helpers')

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load({ path: '.env' })

const app = express()
const paths = config.utils_paths

const hbs = exphbs.create({
  defaultLayout: 'main'
})

app.engine('handlebars', hbs.engine)
app.set('views', __dirname + '/views')
app.set('layouts', __dirname + '/views/')
app.set('view engine', 'handlebars')

// Apply gzip compression
app.use(compress())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(expressValidator())

app.use(express.static(constants.UPLOADS_DIR, { maxAge: 31557600000 }))

const controllers = require('./controllers')
const middleware = require('./middleware')

middleware(app)

app.get('/pages/contracts/:id/pdf', controllers.contract.previewContractPdfById)
app.get('/hopya', (req, res) => {
  return res.send({
    hopya: 'hopya',
    html: 'hey'
  })
})

// This rewrites all routes requests to the root /index.html file
// (ignoring file requests). If you want to implement universal
// rendering, you'll want to remove this middleware.
app.use(require('connect-history-api-fallback')())

const tesseract = require('node-tesseract')
// const TesseractJS = require('tesseract.js')
const Queue = require('bull')
const receiveQueue = Queue('SCAN_OCR')
const processPDFQueue = Queue('PROCESS_PDF')
const dataProvider = require('./models')
const tesseractProcess = Promise.promisify(tesseract.process)

receiveQueue.process((job, done) => {
  const {
    fileObj,
    contractObj,
    fd
  } = job.data

  console.log('Received receiveQueue')

  getTextFromImage(fd).then((text) => {
    let fileData = {
      ocr_data: text,
      updated_at: new Date()
    }
    return dataProvider.File.forge({
      id: fileObj.id
    })
    .save(fileData, { patch: true })
    .catch((error) => {
      console.log('error', error)
    })
    .finally(() => {
      done()
    })
  })
})

// 5 concurrent job
processPDFQueue.process(5, (job, done) => {
  const {
    fileObj,
    contractObj,
    fd
  } = job.data

  console.log('processPDFQueue')

  const SUB_DIR = 'files'
  const DESTINATION_DIR = path.join(constants.UPLOADS_DIR, SUB_DIR)

  return helpers.extractImagesFromPDF(fd, DESTINATION_DIR)
  .then((images) => {
    console.log('images', images.length)
    return Promise.mapSeries(images, (image) => {
      console.log('getTextFromImage', image.path)
      return getTextFromImage(image.path).then((text) => {
        const fileData = {
          url: url.resolve('/', [SUB_DIR, image.name].join('/')),
          name: fileObj.name,
          type: 'image', // fix image tpe
          mimetype: 'image/png', // fix png mimetype
          ocr_data: text,
          contract_id: contractObj.id,
          parent_id: fileObj.id
        }
        return dataProvider.File.forge(fileData).save()
      })
    })
  })
  .finally(() => {
    console.log('process pdf file done.')
    done()
  })
  .catch((err) => {
    console.log('err', err)
  })
})

const getTextFromImage = (imagePath) => {
  return new Promise(function (resolve, reject) {
    tesseract.process(imagePath, (err, text) => {
      if (err) { reject(err) }
      resolve(text)
    })
  })
}
// ------------------------------------
// Apply Webpack HMR Middleware
// ------------------------------------
if (config.env === 'development') {
  const compiler = webpack(webpackConfig)

  debug('Enable webpack dev and HMR middleware')
  app.use(require('webpack-dev-middleware')(compiler, {
    publicPath  : webpackConfig.output.publicPath,
    contentBase : paths.client(),
    hot         : true,
    quiet       : config.compiler_quiet,
    noInfo      : config.compiler_quiet,
    lazy        : false,
    stats       : config.compiler_stats
  }))
  app.use(require('webpack-hot-middleware')(compiler))

  // Serve static assets from ~/src/static since Webpack is unaware of
  // these files. This middleware doesn't need to be enabled outside
  // of development since this directory will be copied into ~/dist
  // when the application is compiled.
  app.use(express.static(paths.client('static')))
} else {
  debug(
    'Server is being run outside of live development mode, meaning it will ' +
    'only serve the compiled application bundle in ~/dist. Generally you ' +
    'do not need an application server for this and can instead use a web ' +
    'server such as nginx to serve your static files. See the "deployment" ' +
    'section in the README for more information on deployment strategies.'
  )

  // Serving ~/dist by default. Ideally these files should be served by
  // the web server and not the app server, but this helps to demo the
  // server in production.
  app.use(express.static(paths.dist()))
}

module.exports = app
