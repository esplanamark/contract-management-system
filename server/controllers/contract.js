const wkhtmltopdf = require('wkhtmltopdf')
const createError = require('http-errors')

const dataProvider = require('../models')

exports.previewContractPdfById = (req, res, next) => {
  const contractId = req.params.id
  const options = {
    withRelated: [ 'meta' ]
  }

  dataProvider.Contract.where({
    id: contractId
  }).fetch(options).then((contract) => {
    if (!contract) {
      return next(createError(404, 'Contract not found.'))
    }

    // return res.render('contract-pdf', { contract: contract.toJSON(), layout: null })

    res.set('Content-type', 'application/pdf')
    // res.set('Content-disposition', 'attachment; filename=pdf.pdf')
    // res.render('contract-pdf', { contract, layout: null }, function (err, html) {
    //   if (err) {
    //     return next(err)
    //   }
    const html = contract.get('html') ? contract.get('html') : '<div style="text-align:center">Empty Template</div>'
    const options = { format: 'Letter' }

    console.log('html', html)
    wkhtmltopdf(html).pipe(res)
  }).catch(err => next(err))
}
