const express = require('express')

const api = require('../api')

const apitRoutes = (middleware) => {
  const apiRouter = express.Router()

  const authenticatePrivate = [
    // middleware.api.authenticateClient,
    middleware.api.authenticateUser,
    middleware.api.requiresAuthorizedUser
  ]

  // contracts routes
  apiRouter.route('/contracts').get(
    authenticatePrivate,
    api.contract.findContracts
  )
  apiRouter.route('/contracts/:id').get(
    authenticatePrivate,
    api.contract.findContracById
  )
  apiRouter.route('/contracts').post(
    authenticatePrivate,
    middleware.upload.array('files', 12),
    api.contract.createContract
  )
  apiRouter.route('/contracts/:id').put(
    authenticatePrivate,
    middleware.upload.array('files', 12),
    api.contract.updateContract
  )
  apiRouter.route('/contracts/:id').delete(
    authenticatePrivate,
    api.contract.destroyContract
  )
  // categories routes
  apiRouter.route('/categories').get(
    authenticatePrivate,
    api.category.findCategory
  )
  apiRouter.route('/categories').post(
    authenticatePrivate,
    api.category.createCategory
  )
  apiRouter.route('/categories/:id').put(
    authenticatePrivate,
    api.category.updateCategory
  )
  apiRouter.route('/categories/:id').delete(
    authenticatePrivate,
    api.category.destroyCategory
  )
  apiRouter.route('/auth/login').post(
    api.auth.login
  )

  apiRouter.use(middleware.api.errorHandler())

  return apiRouter
}

module.exports = apitRoutes
