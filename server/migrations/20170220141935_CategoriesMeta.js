
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable('categories_meta', function (table) {
      table.increments()
      table.integer('category_id').unsigned().notNullable()

      table.string('meta_label', 255)
      table.string('meta_key', 255)
      table.string('meta_type', 255)
      table.string('meta_format', 255)

      table.timestamps()
      table.dateTime('deleted_at')
    })
  ])
}

exports.down = function (knex, Promise) {

}
