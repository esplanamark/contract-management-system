
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable('categories', function (table) {
      table.increments()

      table.text('name')
      table.text('description')
      table.text('meta')
      table.text('html')
      table.text('raw')

      table.timestamps()
      table.dateTime('deleted_at')
    })
  ])
}

exports.down = function (knex, Promise) {

}
