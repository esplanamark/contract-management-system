
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable('contracts_meta', function (table) {
      table.increments()
      table.integer('contract_id').unsigned().notNullable()

      table.string('label', 255)
      table.string('key', 255)
      table.string('value', 255)

      table.timestamps()
      table.dateTime('deleted_at')
    })
  ])
}

exports.down = function (knex, Promise) {

}
