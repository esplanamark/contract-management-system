
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable('contracts', function (table) {
      table.increments()
      table.integer('category_id').unsigned().notNullable()

      table.text('name')
      table.text('description')
      table.text('html')
      table.text('raw')

      table.timestamps()
      table.dateTime('deleted_at')
    })
  ])
}

exports.down = function (knex, Promise) {

}
