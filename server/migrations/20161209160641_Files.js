
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable('files', function (table) {
      table.increments()

      table.integer('contract_id').unsigned().notNullable()
      table.integer('parent_id').unsigned()

      table.string('name')
      table.string('url')
      table.string('image')

      table.string('mimetype', 50)
      table.string('type', 50)
      table.text('ocr_data').collate('utf8mb4')

      table.timestamps()
      table.dateTime('deleted_at')
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('files')
  ])
}
