const path = require('path');

module.exports = {
  PROJECT_DIR: __dirname,
  UPLOADS_DIR: path.join(__dirname, 'uploads'),
  MIMETYPE: {
    image: ['image/jpg', 'image/jpeg', 'image/png'],
    audio: ['audio/mpeg', 'audio/mp4'],
    video: ['video/mp4'],
    pdf: ['application/x-pdf', 'application/pdf']
  },
};
