const config = require('../knexfile')
const knex = require('knex')(config)
const bookshelf = require('bookshelf')(knex)

bookshelf.plugin(require('bookshelf-paranoia'))
bookshelf.plugin('registry')
bookshelf.plugin('pagination')
// bookshelf.plugin('virtuals');
// bookshelf.plugin('visibility');

// knex.migrate.latest();

module.exports = bookshelf
module.exports.knex = knex
