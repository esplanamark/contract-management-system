const bookshelf = require('../config/bookshelf');

const CategoryMeta = bookshelf.Model.extend({
  tableName: 'categories_meta',
  hasTimestamps: true,
  softDelete: true,
});

module.exports = bookshelf.model('CategoryMeta', CategoryMeta);
