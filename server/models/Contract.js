const crypto = require('crypto')
const bookshelf = require('../config/bookshelf')

const Contract = bookshelf.Model.extend({
  tableName: 'contracts',
  hasTimestamps: true,
  softDelete: true,

  files () {
    return this.hasMany('File')
  },

  category () {
    return this.belongsTo('Category')
  },

  meta () {
    return this.hasMany('ContractMeta')
  }
})

module.exports = bookshelf.model('Contract', Contract)
module.exports.Contracts = bookshelf.collection('Contracts', Contract)
