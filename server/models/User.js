const bookshelf = require('../config/bookshelf')

const User = bookshelf.Model.extend({
  tableName: 'users',
  hasTimestamps: true,
  softDelete: true,

  toJSON (options) {
    const _options = options || {}

    let attrs = bookshelf.Model.prototype.toJSON.call(this, _options)

    // remove password hash for security reasons
    delete attrs.password
    delete attrs.passwordResetToken
    delete attrs.passwordResetExpires

    return attrs
  }
})

module.exports = bookshelf.model('User', User)
