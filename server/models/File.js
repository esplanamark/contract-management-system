const bookshelf = require('../config/bookshelf');

const File = bookshelf.Model.extend({
  tableName: 'files',
  hasTimestamps: true,
  softDelete: true,
});

module.exports = bookshelf.model('File', File);
module.exports.Files = bookshelf.collection('Files', File);
