const bookshelf = require('../config/bookshelf');

const ContractMeta = bookshelf.Model.extend({
  tableName: 'contracts_meta',
  hasTimestamps: true,
  softDelete: true,
});

module.exports = bookshelf.model('ContractMeta', ContractMeta);
