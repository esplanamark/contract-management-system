const bookshelf = require('../config/bookshelf');

const Category = bookshelf.Model.extend({
  tableName: 'categories',
  hasTimestamps: true,
  softDelete: true,

  // meta() {
  //   return this.hasMany('CategoryMeta')
  // }
});

module.exports = bookshelf.model('Category', Category);
module.exports.Categories = bookshelf.collection('Categories', Category);
