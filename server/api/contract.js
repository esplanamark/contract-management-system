const Promise = require('bluebird')
const path = require('path')
const url = require('url')
const fs = require('fs')
const fse = require('fs-extra')
const _ = require('lodash')
const createError = require('http-errors')
// const fse =  Promise.promisifyAll(require('fs-extra'))

const helpers = require('../helpers')
const constants = require('../constants')
const dataProvider = require('../models')
const knex = require('../config/bookshelf').knex

const SUB_DIR = 'files'
const DESTINATION_DIR = path.join(constants.UPLOADS_DIR, SUB_DIR)

const Queue = require('bull')
const sendQueue = Queue('SCAN_OCR')
const sendPDFQueue = Queue('PROCESS_PDF')

const processFiles = (files) => {
  return Promise.mapSeries(files, (item) => {
    let fileUrl = url.resolve('/', [SUB_DIR, item.filename].join('/'))
    let fileType = helpers.getFileType(item.mimetype)
    let destinationPath = path.join(DESTINATION_DIR, item.filename)
    return new Promise(function (resolve, reject) {
      fse.copy(item.path, destinationPath, function (err) {
        if (err) { reject(err) }
        resolve()
      })
    }).then(() => {
      return Object.assign({}, item, {
        name: item.originalname,
        url: fileUrl,
        type: fileType,
        mimetype: item.mimetype,
        fd: destinationPath
      })
    })
  })
}

exports.createContract = (req, res, next) => {
  req.assert('name', 'Name cannot be blank').notEmpty()
  req.assert('category_id', 'Category cannot be blank').notEmpty()
  req.assert('category_id', 'Category must be int').isInt()

  const errors = req.validationErrors()

  if (errors) {
    return next(createError(400, 'Bad Request.', {
      err: errors
    }))
  }

  // lets try catch meta, if fails just empt array
  let meta = []
  try {
    meta = req.body.meta ? JSON.parse(req.body.meta) : []
  } catch (e) {
    meta = []
  }

  let raw = {}
  try {
    raw = req.body.raw ? JSON.parse(req.body.raw) : {}
  } catch (e) {
    raw = {}
  }

  dataProvider.Contract.forge({
    name: req.body.name,
    description: req.body.description,
    category_id: req.body.category_id,
    html: req.body.html,
    raw: JSON.stringify(raw)
  }).save()
    .then((newContract) => {
      return new Promise.map(meta, (metaItem) => {
        return dataProvider.ContractMeta.forge({
          contract_id: newContract.id,
          label: metaItem.label,
          value: metaItem.value
        }).save()
      })
      .then(() => {
        return processFiles(req.files).then((formattedFiles) => {
          return new Promise.map(formattedFiles, (file) => {
            return saveFileAndQueue(file, newContract)
            .tap((newFile) => {
              if (newFile.get('type') === 'pdf') {
                console.log('sendPDFQueue')
                return sendPDFQueue.add({
                  contractObj: newContract.toJSON(),
                  fileObj: newFile.toJSON(),
                  fd: file.fd
                }).catch((error) => { console.log('error', error) })
              }
            })
          }).then((finalMappedFiles) => {
            newContract.set('files', finalMappedFiles)
            res.send({
              data: newContract
            })
          })
        })
      })
    })
}

function saveFileAndQueue (file, newContract, parent_id) {
  return dataProvider.File.forge({
    url: file.url,
    image: file.image,
    name: file.originalname,
    type: file.type,
    mimetype: file.mimetype,
    contract_id: newContract.id,
    parent_id
  })
  .save()
  .then((newFile) => {
    if (newFile.get('type') === 'image') {
      return sendQueue.add({
        contractObj: newContract.toJSON(),
        fileObj: newFile.toJSON(),
        fd: file.fd
      }).catch((error) => {
        // TODO:: explain why catch
        // we need to catch to not interrupt the process
        console.log('error', error)
      }).finally(() => {
        return Promise.resolve(newFile)
      })
    }

    return Promise.resolve(newFile)
  })
}

exports.updateContract = (req, res, next) => {
  req.assert('name', 'Name cannot be blank').notEmpty()

  const contractId = req.params.id
  const errors = req.validationErrors()

  dataProvider.Contract
    .where({
      id: contractId
    })
    .fetch()
    .then(function (contract) {
      if (!contract) {
        return next(createError(400, 'Contract not found.'))
      }

      // lets try catch meta, if fails just empt array
      let meta = []
      try {
        meta = req.body.meta ? JSON.parse(req.body.meta) : []
      } catch (e) {
        meta = []
      }

      let raw = {}
      try {
        raw = req.body.raw ? JSON.parse(req.body.raw) : {}
      } catch (e) {
        raw = {}
      }

      // lets try catch meta, if fails just empt array
      let deleted_file_ids = []
      try {
        deleted_file_ids = req.body.deleted_file_ids ? JSON.parse(req.body.deleted_file_ids) : []
      } catch (e) {
        deleted_file_ids = []
      }

      contract.set({
        name: req.body.name,
        description: req.body.description,
        html: req.body.html,
        raw: JSON.stringify(raw)
      })

      if (errors) {
        return next(createError(400, 'Bad Request.', {
          err: errors
        }))
      }

      return contract.save().then(() => {
        return new Promise.map(meta, (metaItem) => {
          return dataProvider.ContractMeta.forge({
            id: metaItem.id
            // contract_id: metaItem.contract_id
          }).save({
            value: metaItem.value
          }, { patch: true })
        }).then(() => {
          return processFiles(req.files).then((formattedFiles) => {
            return new Promise.map(formattedFiles, (file) => {
              return saveFileAndQueue(file, contract)
              .tap((newFile) => {
                if (newFile.get('type') === 'pdf') {
                  console.log('sendPDFQueue')
                  return sendPDFQueue.add({
                    contractObj: contract.toJSON(),
                    fileObj: newFile.toJSON(),
                    fd: file.fd
                  }).catch((error) => { console.log('error', error) })
                }
              })
            }).then(() => {
              return new Promise.map(deleted_file_ids, (fileId) => {
                return dataProvider.File.forge({
                  id: fileId
                }).destroy()
              })
            }).then(() => {
              return res.send({
                data: contract.toJSON()
              })
            })
          })
        })
        // // let formatedCategory = category.toJSON()
        // // formatedCategory.meta = formatedCategory.meta ? JSON.parse(formatedCategory.meta) : '[]'
        //
        // return res.send({
        //   data: contract.toJSON()
        // })
      })
    })
    .catch((err) => next(err))
}

exports.findContracts = (req, res, next) => {
  const {
    page = 1,
    pageSize = 10,
    search = {},
    columns = [],
    order = [],

    sortName,
    sortOrder = 'asc',
    searchText = ''
  } = req.query

  const options = {
    page,
    pageSize,
    withRelated: [ 'meta', 'files' ]
    // debug: true
  }

  return dataProvider.Contract.query((qb) => {
    qb.distinct('contracts.id')
    qb.select('contracts.*')
    qb.select(knex.raw('categories.name as category_name'))

    if (sortName) {
      if (sortName === 'category_name') {
        qb.orderBy('categories.name', sortOrder)
      } else {
        qb.orderBy('contracts.' + sortName, sortOrder)
      }
    } else {
      qb.orderBy('contracts.created_at', 'desc')
    }

    let _searchText = searchText.trim()

    qb.innerJoin('categories', 'contracts.category_id', 'categories.id')
    if (_searchText != '') {
      qb.where(function () {
        this.orWhere(function () {
          this.orWhere('contracts.name', 'like', '%' + _searchText + '%')
          this.orWhere('contracts.description', 'like', '%' + _searchText + '%')
        })
        this.orWhere(function () {
          this.orWhere('files.ocr_data', 'like', '%' + _searchText + '%')
        })
      })

      qb.leftJoin('files', 'contracts.id', 'files.contract_id')
    }
  }).fetchPage(options).then((result) => {
    return result.count().then((countResult) => {
      const finalData = {
        data: result.toJSON(),
        draw: result.pagination.page,
        recordsTotal: countResult,
        recordsFiltered: result.pagination.rowCount
      }

      res.send(finalData)
    })
  })
}

exports.findContracById = (req, res, next) => {
  let options = {
    withRelated: ['files']
  }

  dataProvider.Contract
    .where({
      id: req.params.id
    })
    .fetch(options)
    .then(function (contract) {
      res.send({
        data: contract
      })
    })
    .catch((err) => next(err))
}

exports.destroyContract = (req, res, next) => {
  const contractId = req.params.id
  const errors = req.validationErrors()

  dataProvider.Contract
    .where({
      id: contractId
    })
    .fetch()
    .then(function (contract) {
      if (!contract) {
        return next(createError(400, 'Contract not found.'))
      }

      return contract.destroy().then(() => {
        return res.send({
          data: ''
        })
      })
    })
    .catch((err) => next(err))
}
