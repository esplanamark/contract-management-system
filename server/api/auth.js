const createError = require('http-errors')
const Promise = require('bluebird')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const helpers = require('../helpers')
const constants = require('../constants')
const dataProvider = require('../models')

let bcryptGenSalt = Promise.promisify(bcrypt.genSalt),
  bcryptHash = Promise.promisify(bcrypt.hash),
  bcryptCompare = Promise.promisify(bcrypt.compare)

const validatePasswordLength = (password) => {
  return validator.isLength(password, 8)
}

const generatePasswordHash = (password) => {
  // Generate a new salt
  return bcryptGenSalt().then((salt) => {
    // Hash the provided password with bcrypt
    return bcryptHash(password, salt)
  })
}

exports.login = (req, res, next) => {
  req.assert('username', 'Username cannot be blank').notEmpty()
  req.assert('password', 'Password cannot be blank').notEmpty()

  const errors = req.validationErrors()

  if (errors) {
    return next(createError(400, 'Bad Request.', {
      err: errors
    }))
  }

  const {
    username,
    password
  } = req.body

  dataProvider.User.forge({
    email: username
  }).fetch().then((user) => {
    if (!user) {
      return next(createError(400, 'User not found.'))
    }

    return bcryptCompare(password, user.get('password')).then((matched) => {
      if (!matched) {
        return next(createError(400, 'Authentication failed. Wrong password.'))
      }

      // if user is found and password is right
      // create a token
      var token = jwt.sign(user.toJSON(), 'itssecret_', {
        expiresIn: 60 * 60 * 60
      })

      res.json({
        data: {
          token: token,
          user: user.toJSON()
        }
      })
    })
  }).catch(error => next(error))
}
