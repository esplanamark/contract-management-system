const createError = require('http-errors')

const helpers = require('../helpers')
const constants = require('../constants')
const dataProvider = require('../models')

exports.createCategory = (req, res, next) => {
  req.assert('name', 'Name cannot be blank').notEmpty()

  const errors = req.validationErrors()

  if (errors) {
    return next(createError(400, 'Bad Request.', {
      err: errors
    }))
  }

  const meta = req.body.meta ? JSON.stringify(req.body.meta) : '[]'

  dataProvider.Category.forge({
    name: req.body.name,
    description: req.body.description,
    html: req.body.html,
    raw: req.body.raw,
    meta
  }).save()
    .then((newCategory) => {
      res.send({
        data: newCategory
      })
    })
}

exports.updateCategory = (req, res, next) => {
  req.assert('name', 'Name cannot be blank').notEmpty()

  const categoryId = req.params.id
  const errors = req.validationErrors()

  dataProvider.Category
    .where({
      id: categoryId
    })
    .fetch()
    .then(function (category) {
      if (!category) {
        return next(createError(400, 'Category not found.'))
      }

      const meta = req.body.meta ? JSON.stringify(req.body.meta) : '[]'

      category.set({
        name: req.body.name,
        description: req.body.description,
        html: req.body.html,
        raw: req.body.raw,
        meta
      })

      if (errors) {
        return next(createError(400, 'Bad Request.', {
          err: errors
        }))
      }

      return category.save().then(() => {
        let formatedCategory = category.toJSON()
        formatedCategory.meta = formatedCategory.meta ? JSON.parse(formatedCategory.meta) : '[]'

        return res.send({
          data: formatedCategory
        })
      })
    })
    .catch((err) => next(err))
}

exports.destroyCategory = (req, res, next) => {
  const categoryId = req.params.id
  const errors = req.validationErrors()

  dataProvider.Category
    .where({
      id: categoryId
    })
    .fetch()
    .then(function (category) {
      if (!category) {
        return next(createError(400, 'Category not found.'))
      }

      return category.destroy().then(() => {
        return res.send({
          data: ''
        })
      })
    })
    .catch((err) => next(err))
}

exports.findCategory = (req, res, next) => {
  const {
    page = 1,
    pageSize = 10,
    search = {},
    columns = [],
    order = [],

    sortName,
    sortOrder = 'asc',
    searchText = ''
  } = req.query

  const options = {
    page,
    pageSize
    // withRelated: [ 'meta' ]
  }

  return dataProvider.Category.query((qb) => {
    if (sortName) {
      qb.orderBy(sortName, sortOrder)
    } else {
      qb.orderBy('created_at', 'desc')
    }

    let _searchText = searchText.trim()

    if (_searchText != '') {
      qb.orWhere(function () {
        this.orWhere('name', 'like', '%' + _searchText + '%')
      })
    }
  }).fetchPage(options).then((result) => {
    return result.count().then((countResult) => {
      let data = result.toJSON()

      data = data.map((d) => {
        return Object.assign({}, d, {
          meta: d.meta ? JSON.parse(d.meta) : []
        })
      })

      const finalData = {
        data: data,
        draw: result.pagination.page,
        recordsTotal: countResult,
        recordsFiltered: result.pagination.rowCount
      }

      res.send(finalData)
    })
  })
}
