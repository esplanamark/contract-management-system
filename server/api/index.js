const auth = require('./auth')
const category = require('./category')
const contract = require('./contract')

module.exports = {
  auth,
  category,
  contract
}
