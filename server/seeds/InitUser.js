
exports.seed = function (knex, Promise) {
  return Promise.all([
    // Inserts seed entries
    knex('users').insert({
      id: 1,
      name: 'Super User',
      email: 'contract@@vibalgroup.com',
      password: '$2a$10$bjs77v3i9xsHeGH5F.dbNuEi1cV7UKfAXGbVqjvW9TLoWFS/MVm2a' // 12345678
    })
  ])
}
